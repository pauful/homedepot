import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
import string
import re
from nltk.corpus import stopwords
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import wordnet as wn
from sets import Set
import enchant

df_all = joblib.load('df_all3.txt')

df_all.to_csv('dataTocheck.csv',index=False)

print df_all[['search_term','product_info_stem','product_info_spell']]

# print 'Calcular words en stem search + title o desc'
# df_all['word_in_title_stem'] = df_all['product_info_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
# df_all['word_in_description_stem'] = df_all['product_info_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[2]))
# print 'Calcular words en stem search + atts'
# df_all['word_in_attributes_stem'] = df_all['search_att_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
#
# print 'Syms for stemming'
#
# df_all['word_with_synonims_desc'] = df_all['product_info_spell'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[2]))
# df_all['word_with_synonims_title'] = df_all['product_info_spell'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
# df_all['word_with_synonims_att'] = df_all['search_att_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
#
# joblib.dump(df_all, 'df_all3.txt')
# print 'Syms for spell'
#
# df_all['word_with_synonims_desc_spell'] = df_all['product_info_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[2]))
# df_all['word_with_synonims_title_spell'] = df_all['product_info_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
# df_all['word_with_synonims_att_spell'] = df_all['search_att_spell'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
#

#
#
# df_all = df_all.drop(['search_term','product_title','product_description', 'search_att', 'attrs',
#     'search_term_stem','product_title_stem','product_description_stem','product_info_stem', 'search_att_stem', 'attrs_stem',
#     'product_all_info','search_att_spell','product_info_spell','search_spell_check','search_and_all_info'],axis=1)
# print 'ay'
# df_train = df_all.iloc[:num_train]
# df_test = df_all.iloc[num_train:]
# id_test = df_test['id']
#
# y_train = df_train['relevance'].values
# X_train = df_train.drop(['id','relevance'],axis=1).values
# X_test = df_test.drop(['id','relevance'],axis=1).values
# print 'ay'
# rf = RandomForestRegressor(n_estimators=15, max_depth=6, random_state=0)
# clf = BaggingRegressor(rf, n_estimators=45, max_samples=0.1, random_state=25)
# clf.fit(X_train, y_train)
# y_pred = clf.predict(X_test)
# print 'ay'
# pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
# print 'ay'
