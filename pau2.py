import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor, AdaBoostRegressor
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
from sklearn import cross_validation
from sklearn import pipeline, metrics, grid_search
from sklearn.metrics import mean_squared_error
import xgboost as xgb
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDRegressor
from sklearn.svm import SVR
from sklearn.ensemble import ExtraTreesRegressor

def mean_sq_error(y, y_pred):
    return mean_squared_error(y, y_pred)**0.5

df_train = pd.read_csv('train.csv', encoding="ISO-8859-1")
df_test = pd.read_csv('test.csv', encoding="ISO-8859-1")
# df_attr = pd.read_csv('../input/attributes.csv')
df_pro_desc = pd.read_csv('product_descriptions.csv')

df_attr = pd.read_csv('pau-atts2.csv')



num_train = df_train.shape[0]

df_all = joblib.load('df_all4.txt')

print df_all([['product_title','product_title_norm','product_title_norm_stem']])

df_all = df_all.drop([ 'product_title', u'search_term', 'product_description', 'attrs', 'search_term_norm',
 'product_title_norm', 'product_description_norm', 'attrs_norm', 'search_and_all_norm', 'search_term_norm_stem',
 'product_title_norm_stem', 'product_description_norm_stem', 'attrs_norm_stem', 'search_norm_spell', 'search_and_all_norm_spell',
 'search_and_all_norm_syn_stem'],axis=1)

print df_all.columns.names

print "headers"
print list(df_all.columns.values)
df_train = df_all.iloc[:num_train]
df_test = df_all.iloc[num_train:]
id_test = df_test['id']

y_train = df_train['relevance']
X_train = df_train.drop(['id','relevance'],axis=1)
X_test = df_test.drop(['id','relevance'],axis=1)


from sklearn.base import TransformerMixin
class DataFrameImputer(TransformerMixin):
    def fit(self, X, y=None):
        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].median() for c in X],
            index=X.columns)
        return self
    def transform(self, X, y=None):
        return X.fillna(self.fill)

big_X_imputed = DataFrameImputer().fit_transform(df_train.drop(['id','relevance'],axis=1))

X_train = big_X_imputed.as_matrix()

big_T_imputed = DataFrameImputer().fit_transform(df_test.drop(['id','relevance'],axis=1))

X_test = big_T_imputed.as_matrix()

print X_train

rf = RandomForestRegressor(n_estimators=55, max_depth=6, random_state=0)
clf = BaggingRegressor(rf, n_estimators=45, max_samples=0.1, random_state=25)
sgd = SGDRegressor()
eetr = ExtraTreesRegressor()
from sklearn.ensemble import RandomTreesEmbedding
from sklearn.ensemble import GradientBoostingRegressor
from  sklearn.preprocessing import MinMaxScaler

est = pipeline.Pipeline([
# ('mmsc', MinMaxScaler()),
    # ('embedding', RandomTreesEmbedding()),
    # ('sc', StandardScaler()),

    # ('svr', SVR())
    # ('sgd', sgd)
    # ('xgb', xgb.XGBRegressor(base_score=0.9, colsample_bylevel=1, colsample_bytree=1, gamma=0,
    #    learning_rate=0.05, max_delta_step=0, max_depth=3,
    #    min_child_weight=1, missing=None, n_estimators=200, nthread=-1,
    #    objective='reg:linear', reg_alpha=0, reg_lambda=1,
    #    scale_pos_weight=1, seed=0, silent=True, subsample=1))
    # ('gradient', GradientBoostingRegressor())
    # ('eetr', eetr)
    ('bagging', clf)
])

# Create a parameter grid to search for best parameters for everything in the pipeline
param_grid = {
    'bagging__n_estimators': [45, 100, 150],
    'bagging__max_samples': [0.1, 0.3, 0.4]

    # 'sgd__loss': ['squared_loss', 'huber'],
    # 'sgd__penalty': ['l2','l1','elasticnet'],
    # 'sgd__alpha': [0.0001, 0.001],
    # 'sgd__n_iter': [5, 10, 20]

    # 'svr__C': [1.2, 1, 0.5],
    # 'svr__epsilon': [0.1, 0.3, 0.5],
    # 'svr__kernel': ['rbf', 'linear']

    # 'sc__with_mean': [False],

    # 'embedding__n_estimators': [700],
    # 'xgb__n_estimators': [200],
    # 'xgb__max_depth': [5, 8],
    # 'xgb__learning_rate': [0.05],
    # 'xgb__base_score': [0.5, 0.7]

    # 'xgb__objective': ['reg:logistic']
    # 'eetr__n_estimators': [1000, 2000],
    # 'eetr__warm_start': [True]

    # 'gradient__loss': ['ls', 'lad', 'huber', 'quantile'],
    # 'gradient__learning_rate': [0.1, 0.5, 0.001],
    # 'gradient__n_estimators': [100, 200, 500],
    # 'gradient__max_depth': [10, 5]
}

mean_error = metrics.make_scorer(mean_sq_error, greater_is_better = False)

# Initialize Grid Search Model
model = grid_search.GridSearchCV(estimator  = est,
         param_grid = param_grid,
         scoring    = mean_error,
         verbose    = 10,
         n_jobs     = 4,
         iid        = True,
         refit      = True,
         cv         = 2)
# print X_train

# Fit Grid Search Model
model.fit(X_train, y_train)
print("Best score: %0.3f" % model.best_score_)
print("Best parameters set:")
best_parameters = model.best_estimator_.get_params()
for param_name in sorted(param_grid.keys()):
    print("\t%s: %r" % (param_name, best_parameters[param_name]))

# Get best model

best_model = model.best_estimator_
print best_model
# Fit model with best parameters optimized for normalized_gini
# best_model.fit(X_train, y_train)
y_pred = best_model.predict(X_test)
print 'ay'
pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submissioneNew.csv',index=False)
print 'ay'
