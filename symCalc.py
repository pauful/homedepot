import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
import string
import re
from nltk.corpus import stopwords
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import wordnet as wn
from sets import Set
import enchant
import sys

nltk.download('stopwords')
d = enchant.Dict("en_US")
cachedStopWords = stopwords.words("english")

stemmer = SnowballStemmer('english')

df_train = pd.read_csv('train.csv', encoding="ISO-8859-1")
df_test = pd.read_csv('test.csv', encoding="ISO-8859-1")
# df_attr = pd.read_csv('../input/attributes.csv')
df_pro_desc = pd.read_csv('product_descriptions.csv')

from sklearn.feature_extraction.text import TfidfVectorizer
corpus = df_pro_desc['product_description']
vectorizer = TfidfVectorizer(min_df=1)
X = vectorizer.fit_transform(corpus)
idf = vectorizer.idf_
product_description_dict = dict(zip(vectorizer.get_feature_names(), idf))

df_attr = pd.read_csv('pau-atts2.csv')

# df_attr_grouped = df_attr.groupby('product_uid')['value'].agg(lambda x:''.join(str(x)))
# df_attr_grouped = df_attr.groupby('product_uid')['value'].agg(lambda x:','.join(str(x)))

# df_attr_grouped.to_csv('pau-atts.csv')
#
# print df_attr_grouped.iloc[:30]


num_train = df_train.shape[0]

syms_words = {}

def spaceNumber(text):
    newtext = ''
    n = False
    for l in text:
        if l in string.digits:
            if n:
                n = True
                newtext = newtext+l
            else:
                n = True
                newtext = newtext+' '+l
        else :
            if n:
                newtext = newtext +  ' '+l
                n = False
            else:
                newtext = newtext + l
                n = False
    return newtext

def separateUpperCase(text):
    newtext = ''
    n = False
    for l in text:
        if l.isupper():
            if n:
                n = False
                newtext = newtext+ ' ' +l
            else:
                n = False
                newtext = newtext+l
        else :
            newtext = newtext + l
            n = True
    return newtext

def calc_syms(text):
    text = spaceNumber(text)
    text = separateUpperCase(text)
    text = re.sub('[%s]' % string.punctuation,' ',text)

    for word2 in text.split():
        # print word2.isdigit
        if word2 not in syms_words:

            if word2.isdigit() == False:
                # print 'in'

                a = []
                for ss in wn.synsets(word2):
                    # newLemmas = [stemmer.stem(lema) for lema in ss.lemma_names()]
                    a = a + ss.lemma_names()
                syms_words[word2] = a


print 'ay'
df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)

df_all = pd.merge(df_all, df_pro_desc, how='left', on='product_uid')

# print pd.merge(df_all, df_attr_grouped, how='left', on='product_uid')

# print df_all.iloc[:3]
df_attr['product_uid'].apply(float)

# df_all = df_all.join(df_attr, on='product_uid')
df_all = pd.merge(df_all, df_attr, how='left', on='product_uid')
# df_all = pd.merge(df_all, df_attr, how='left', on='product_uid')
# print df_all['value'].iloc[:300]
print "//////--------------------~~~~~~~~~~~~~~~~~---------------"
# df_all['attributes'] = df_all.groupby('product_uid')['value'].agg(lambda x:''.join(str(x)))
#
# print df_all.iloc[:30]

# df_all['search_att'] = df_all['attributes'] + '\t' + df_all['search_term']

print 'Normal columns'
df_all['attrs'] = df_all['attrs'].map(lambda x:str(x).decode("utf8"))
print 'Decoded'
df_all['attrs'].map(lambda x:calc_syms(x))
print 'Atts done'
df_all['product_title'].map(lambda x:calc_syms(x))
print 'Title done'
df_all['product_description'].map(lambda x:calc_syms(x))
print 'Desc Done'

print syms_words

joblib.dump(syms_words, 'symsFile.txt')
