import enchant
import pandas as pd
import re
import string

df_train = pd.read_csv('train.csv', encoding="ISO-8859-1")
df_test = pd.read_csv('test.csv', encoding="ISO-8859-1")

listM = {

}

d = enchant.Dict("en_UK")

def spaceNumber(text):
    newtext = ''
    n = False
    for l in text:
        if l in string.digits:
            if n:
                n = True
                newtext = newtext+l
            else:
                n = True
                newtext = newtext+' '+l
        else :
            if n:
                newtext = newtext +  ' '+l
                n = False
            else:
                newtext = newtext + l
                n = False
    return newtext

def addToMispelled(s):
    s = s.lower()
    s = re.sub('[%s]' % string.punctuation,' ',s)
    s = spaceNumber(s)
    for w in s.split():
        if d.check(w) != True:
            if w not in listM:
                listM[w] = d.suggest(w)


df_train['search_term'] = df_train['search_term'].map(lambda x:addToMispelled(x))

print list(listM.iterkeys())
print len(listM)
