import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
import string
import re
from nltk.corpus import stopwords
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import wordnet as wn
from sets import Set
from nltk.corpus import wordnet as wn

nltk.download('stopwords')

cachedStopWords = stopwords.words("english")

stemmer = SnowballStemmer('english')

df_train = pd.read_csv('train.csv', encoding="ISO-8859-1")
df_test = pd.read_csv('test.csv', encoding="ISO-8859-1")

num_train = df_train.shape[0]

def findNumWordsWithSyms(text1, text2):
    # print text1
    # print text2
    s = 0
    # print text1
    # print text2
    for word1 in text1.split():
        if word1.isdigit() == False:
            # print 'imm'
            for word2 in text2.split():
                # print word2.isdigit
                if word2.isdigit() == False:
                    # print 'in'
                    a = []

                    for ss in wn.synsets(word2):
                        a = a + ss.lemma_names()
                    a.append(word2)
                    if word1 in a:
                        s += 1
    # if s != 0:
    #     print s
        if word1.isdigit() == False:
            # print 'imm'
            if word1 in text2.split():
                s += 1

    # print s
    # print '/////////'
    return s

def spaceNumber(text):
    newtext = ''
    n = False
    for l in text:
        if l in string.digits:
            if n:
                n = True
                newtext = newtext+l
            else:
                n = True
                newtext = newtext+' '+l
        else :
            if n:
                newtext = newtext +  ' '+l
                n = False
            else:
                newtext = newtext + l
                n = False
    return newtext

def str_stemmer(s):
	s = s.lower()
	s = re.sub('[%s]' % string.punctuation,' ',s)
	s = spaceNumber(s)
	return " ".join([stemmer.stem(word) for word in s.lower().split() if word not in cachedStopWords])

def str_common_word(str1, str2):
	return sum(int(str2.find(word)>=0) for word in str1.split())

def str_common_word_desc(str1, str2, d):
	return sum(int(str2.find(word)>=0) * d[word] for word in str1.split() if word in d)

df_all = joblib.load('df_all2.txt')
print 'aaaaa'
df_all['word_with_synonims_desc'] = df_all['product_info'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[2]))
df_all['word_with_synonims_title'] = df_all['product_info'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
df_all['word_with_synonims_att'] = df_all['search_att'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))


print 'aaaaallll'
print df_all.iloc[:3]

joblib.dump(df_all, 'df_all2.txt')


df_all = df_all.drop(['search_term','product_title','product_description','product_info', 'search_att', 'attrs'],axis=1)
print 'ay'
df_train = df_all.iloc[:num_train]
df_test = df_all.iloc[num_train:]
id_test = df_test['id']

y_train = df_train['relevance'].values
X_train = df_train.drop(['id','relevance'],axis=1).values
X_test = df_test.drop(['id','relevance'],axis=1).values
print 'ay'
rf = RandomForestRegressor(n_estimators=15, max_depth=6, random_state=0)
clf = BaggingRegressor(rf, n_estimators=45, max_samples=0.1, random_state=25)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print 'ay'
pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
print 'ay'
