import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
import string
import re
from nltk.corpus import stopwords
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import wordnet as wn
from sets import Set
import enchant
import sys

nltk.download('stopwords')
d = enchant.Dict("en_US")
cachedStopWords = stopwords.words("english")

stemmer = SnowballStemmer('english')

df_train = pd.read_csv('train.csv', encoding="ISO-8859-1")
df_test = pd.read_csv('test.csv', encoding="ISO-8859-1")
# df_attr = pd.read_csv('../input/attributes.csv')
df_pro_desc = pd.read_csv('product_descriptions.csv')

from sklearn.feature_extraction.text import TfidfVectorizer
corpus = df_pro_desc['product_description']
vectorizer = TfidfVectorizer(min_df=1)
X = vectorizer.fit_transform(corpus)
idf = vectorizer.idf_
product_description_dict = dict(zip(vectorizer.get_feature_names(), idf))

df_attr = pd.read_csv('pau-atts2.csv')

syms_dict = joblib.load('symsFile.txt')


num_train = df_train.shape[0]

def str_medidas(s):

    s = s.replace("'"," in. ") # character
    s = s.replace("inches"," in. ") # whole word
    s = s.replace("inch"," in. ") # whole word
    s = s.replace(" in "," in. ") # no period
    s = s.replace(" in."," in. ") # prefix space

    s = s.replace("''"," ft. ") # character
    s = s.replace(" feet "," ft. ") # whole word
    s = s.replace("feet"," ft. ") # whole word
    s = s.replace("foot"," ft. ") # whole word
    s = s.replace(" ft "," ft. ") # no period
    s = s.replace(" ft."," ft. ") # prefix space

    s = s.replace(" pounds "," lb. ") # character
    s = s.replace(" pound "," lb. ") # whole word
    s = s.replace("pound", " lb. ") # whole word
    s = s.replace(" lb "," lb. ") # no period
    s = s.replace(" lb."," lb. ")
    s = s.replace(" lbs "," lb. ")
    s = s.replace("lbs."," lb. ")

    s = s.replace("*"," xby ")
    s = s.replace(" by"," xby ")
    s = s.replace("x0"," xby 0 ")
    s = s.replace("x1"," xby 1 ")
    s = s.replace("x2"," xby 2 ")
    s = s.replace("x3"," xby 3 ")
    s = s.replace("x4"," xby 4 ")
    s = s.replace("x5"," xby 5 ")
    s = s.replace("x6"," xby 6 ")
    s = s.replace("x7"," xby 7 ")
    s = s.replace("x8"," xby 8 ")
    s = s.replace("x9"," xby 9 ")

    s = s.replace(" sq ft"," sq.ft. ")
    s = s.replace("sq ft"," sq.ft. ")
    s = s.replace("sqft"," sq.ft. ")
    s = s.replace(" sqft "," sq.ft. ")
    s = s.replace("sq. ft"," sq.ft. ")
    s = s.replace("sq ft."," sq.ft. ")
    s = s.replace("sq feet"," sq.ft. ")
    s = s.replace("square feet"," sq.ft. ")

    s = s.replace(" gallons "," gal. ") # character
    s = s.replace(" gallon "," gal. ") # whole word
    s = s.replace("gallons"," gal. ") # character
    s = s.replace("gallon"," gal. ") # whole word
    s = s.replace(" gal "," gal. ") # character
    s = s.replace(" gal"," gal. ") # whole word

    s = s.replace(" ounces"," oz. ")
    s = s.replace(" ounce"," oz. ")
    s = s.replace("ounce"," oz. ")
    s = s.replace(" oz "," oz. ")

    s = s.replace(" centimeters"," cm. ")
    s = s.replace(" cm."," cm. ")
    s = s.replace(" cm "," cm. ")

    s = s.replace(" milimeters"," mm. ")
    s = s.replace(" mm."," mm. ")
    s = s.replace(" mm "," mm. ")
    return s

def separateUpperCase(text):
    newtext = ''
    n = False
    for l in text:
        if l.isupper():
            if n:
                n = False
                newtext = newtext+ ' ' +l
            else:
                n = False
                newtext = newtext+l
        else :
            newtext = newtext + l
            n = True
    return newtext

def spaceNumber(text):
    newtext = ''
    n = False
    for l in text:
        if l in string.digits:
            if n:
                n = True
                newtext = newtext+l
            else:
                n = True
                newtext = newtext+' '+l
        else :
            if n:
                newtext = newtext +  ' '+l
                n = False
            else:
                newtext = newtext + l
                n = False
    return newtext


def str_normalizar(s):
    s = separateUpperCase(s)
    s = spaceNumber(s)
    s = str_medidas(s)
    text = " ".join([word for word in s.split() if word not in cachedStopWords])
    return text

def isFloat(s):
    try:
        f = float(s)
        return True
    except Exception as e:
        return False


from fractions import Fraction

def isFraction(s):
    try:
        f = Fraction(s)
        return True
    except ValueError as e:
        return False

def str_stemmer(s):
    s = s.lower()
    s = s.replace(',','.')
    text = ''
    for word in s.split():
        if not isFloat(word) and not isFraction(word):
            w = re.sub('[%s]' % string.punctuation,' ',word)

            text = text + ' ' + stemmer.stem(w)
        else:
            text = text + ' ' + word
    return text

def str_common_word(str1, str2):
	return sum(int(str2.find(word)>=0) for word in str1.split())

def str_common_word_desc(str1, str2, d):
	return sum(int(str2.find(word)>=0) * d[word] for word in str1.split() if word in d)


def spell_check(str1, str2):

    newText = ''

    for w in str1.split():

        if str2.find(w)<0:

            if not d.check(w):
                # print 'not valid'

                listM = d.suggest(w)
                if len(listM) > 0:
                    # print w + ' = ' + listM[0]
                    newText += newText + ' ' + listM[0]
                else:
                    # print w + ' = ' + stemmer.stem(w)
                    newText += newText + ' ' + w
            else:
                newText += ' ' + w

    return newText

def addSynonims(text):

    nText = ''

    for w in text.split():
        if w in syms_dict:
            nText = nText + " ".join([ ss.replace('_',' ') for ss in syms_dict[w]])
        else:
            nText = nText + ' ' + w

    return nText

# print 'Initialize'
#
# df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)
#
# df_all = pd.merge(df_all, df_pro_desc, how='left', on='product_uid')
#
# df_attr['product_uid'].apply(float)
#
# df_all = pd.merge(df_all, df_attr, how='left', on='product_uid')
#
# print 'Normal columns'
#
# df_all['attrs'] = df_all['attrs'].map(lambda x:str(x).decode("utf8"))
#
# df_all['search_term_norm'] = df_all['search_term'].map(lambda x:str_normalizar(x))
# df_all['product_title_norm'] = df_all['product_title'].map(lambda x:str_normalizar(x))
# df_all['product_description_norm'] = df_all['product_description'].map(lambda x:str_normalizar(x))
# df_all['attrs_norm'] = df_all['attrs'].map(lambda x:str_normalizar(x))
#
# df_all['search_and_all_norm'] = df_all['search_term_norm'] +"\t"+df_all['product_title_norm']+"\t"+df_all['product_description_norm']+"\t"+df_all['attrs_norm']
#
# joblib.dump(df_all, 'df_allU.txt')
#
# print df_all.iloc[:3]
#
# print 'Calcular stem'
#
# df_all['search_term_norm_stem'] = df_all['search_term_norm'].map(lambda x:str_stemmer(x))
# df_all['product_title_norm_stem'] = df_all['product_title_norm'].map(lambda x:str_stemmer(x))
# df_all['product_description_norm_stem'] = df_all['product_description_norm'].map(lambda x:str_stemmer(x))
# df_all['attrs_norm_stem'] = df_all['attrs_norm'].map(lambda x:str_stemmer(x))
#
# df_all['search_and_all_norm'] = df_all['search_term_norm_stem'] +"\t"+df_all['product_title_norm_stem']+"\t"+df_all['product_description_norm_stem']+"\t"+df_all['attrs_norm_stem']
#
# joblib.dump(df_all, 'df_allU.txt')
#
# print df_all.iloc[:3]
#
# print 'Calcular search spell'
#
# df_all['search_norm_spell'] = df_all['search_and_all_norm'].map(lambda x:spell_check(x.split('\t')[0],x.split('\t')[1]+' '+x.split('\t')[2]+' '+x.split('\t')[3]))
#
# df_all['search_and_all_norm_spell'] = df_all['search_norm_spell'] +"\t"+df_all['product_title_norm_stem']+"\t"+df_all['product_description_norm_stem']+"\t"+df_all['attrs_norm_stem']
#
# joblib.dump(df_all, 'df_allU.txt')
#
# print df_all.iloc[:3]
#
# print 'Calcular common words stem'
#
# df_all['num_common_title_stem'] = df_all['search_and_all_norm'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
# df_all['num_common_desc_stem']  = df_all['search_and_all_norm'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[2]))
# df_all['num_common_atts_stem']  = df_all['search_and_all_norm'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[3]))
#
# joblib.dump(df_all, 'df_allU.txt')
#
# print df_all.iloc[:3]
#
# print 'Calcular common words stem+spell'
#
# df_all['num_common_title_stem_spell'] = df_all['search_and_all_norm_spell'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
# df_all['num_common_desc_stem_spell']  = df_all['search_and_all_norm_spell'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[2]))
# df_all['num_common_atts_stem_spell']  = df_all['search_and_all_norm_spell'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[3]))
#
# joblib.dump(df_all, 'df_allU.txt')
#
# print df_all.iloc[:3]
#
# print 'Calcular common words stem * TiDf'
#
# df_all['num_common_title_stem_tf_idf'] = df_all['search_and_all_norm'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[1], product_description_dict))
# df_all['num_common_desc_stem_tf_idf'] = df_all['search_and_all_norm'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[2], product_description_dict))
# df_all['num_common_atts_stem_tf_idf'] = df_all['search_and_all_norm'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[3], product_description_dict))
#
# joblib.dump(df_all, 'df_allU.txt')
#
# print df_all.iloc[:3]
#
# print 'Calcular common words stem + spell * TiDf'
#
# df_all['num_common_title_stem_spell_tf_idf'] = df_all['search_and_all_norm_spell'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[1], product_description_dict))
# df_all['num_common_desc_stem_spell_tf_idf'] = df_all['search_and_all_norm_spell'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[2], product_description_dict))
# df_all['num_common_atts_stem_spell_tf_idf'] = df_all['search_and_all_norm_spell'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[3], product_description_dict))
#
# joblib.dump(df_all, 'df_allU.txt')
#
# print df_all.iloc[:3]



print 'Calcular num with Syms + stemming'

# df_all['word_with_synonims_desc'] = df_all['product_info_spell'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[2]))
# df_all['word_with_synonims_title'] = df_all['product_info_spell'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
# df_all['word_with_synonims_att'] = df_all['search_att_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
#
# joblib.dump(df_all, 'df_all4.txt')
#
# print 'Syms for spell'
#
# df_all['word_with_synonims_desc_spell'] = df_all['product_info_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[2]))
# df_all['word_with_synonims_title_spell'] = df_all['product_info_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
# df_all['word_with_synonims_att_spell'] = df_all['search_spell_check_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
#
# joblib.dump(df_all, 'df_all4.txt')

df_all = joblib.load('df_all4.txt')

# print 'Calcular syns'
#
# df_all['product_title_norm_syn_stem'] = df_all['product_title_norm'].map(lambda x:addSynonims(x))
# print 'Title syns calculated'
# df_all['product_description_norm_syn_stem'] = df_all['product_description_norm'].map(lambda x:addSynonims(x))
# print 'Title syns calculated'
# df_all['attrs_norm_syn_stem'] = df_all['attrs_norm'].map(lambda x:addSynonims(x))
#
#
# print 'Atts syns calculated'
#
# df_all['search_and_all_norm_syn_stem'] = df_all['search_term_norm_stem'] +"\t"+df_all['product_title_norm_syn_stem']+"\t"+df_all['product_description_norm_syn_stem']+"\t"+df_all['attrs_norm_syn_stem']
#
# df_all = df_all.drop(['product_title_norm_syn_stem', 'product_description_norm_syn_stem',
#     'attrs_norm_syn_stem'],axis=1)
#
# joblib.dump(df_all, 'df_all4.txt')
#
print 'Calcular syns num stem'

df_all['num_common_title_stem_syn'] = df_all['search_and_all_norm_syn_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
df_all['num_common_desc_stem_syn']  = df_all['search_and_all_norm_syn_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[2]))
df_all['num_common_atts_stem_syn']  = df_all['search_and_all_norm_syn_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[3]))

# joblib.dump(df_all, 'df_all4.txt')

print 'Calcular syns num stem tf idf'

df_all['num_common_title_stem_syn_tf_idf'] = df_all['search_and_all_norm_syn_stem'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[1], product_description_dict))
df_all['num_common_desc_stem_syn_tf_idf'] = df_all['search_and_all_norm_syn_stem'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[2], product_description_dict))
df_all['num_common_atts_stem_syn_tf_idf'] = df_all['search_and_all_norm_syn_stem'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[3], product_description_dict))

joblib.dump(df_all, 'df_all4.txt')

# df_all = df_all.drop(['search_and_all_norm_syn_stem','product_title_norm_syn_stem', 'product_description_norm_syn_stem',
#     'attrs_norm_syn_stem'],axis=1)
#
# joblib.dump(df_all, 'df_all4.txt')

print list(df_all.columns.values)

print df_all.iloc[:3]
