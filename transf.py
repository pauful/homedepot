#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import json
import re

csvfile = open('../input/attributes.csv', 'r')

listP = {}

fieldnames = ("product_uid","name","value")
reader = csv.DictReader( csvfile, fieldnames)
i = 0
for row in reader:

    try:
        if row['product_uid'] not in listP:

            listP[row['product_uid']] = {
                'product_uid': int(row['product_uid']),
                'bullets': '',
                'sizes': '',
                'thingsYes': '',
                'thingsNo': ''
            }
        if row['name'].startswith('Bullet'):
            listP[row['product_uid']]['bullets'] = listP[row['product_uid']]['bullets'] + ' ' + row['value']

        if row['value'] == 'Yes':
            listP[row['product_uid']]['thingsYes'] = listP[row['product_uid']]['thingsYes'] + " " + row['name']

        if row['value'] == 'No':
            listP[row['product_uid']]['thingsNo'] = listP[row['product_uid']]['thingsNo'] + " " + row['name']

        la = re.match(r'.* \((.*)\)', row['name'] )
        if la:
            listP[row['product_uid']]['sizes'] = listP[row['product_uid']]['sizes'] + " " + row['name'] + la.group(1)

    except ValueError as e:
            print 'hh'+row['product_uid']+'hh'




print list(listP.values())[0]

with open('pau-atts2.csv', 'wb') as output_file:
    dict_writer = csv.DictWriter(output_file, ['product_uid', 'bullets', 'sizes', 'thingsYes', 'thingsNo'])
    dict_writer.writeheader()
    dict_writer.writerows(list(listP.values()))
