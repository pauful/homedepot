import string
from nltk.corpus import stopwords
import re

cachedStopWords = stopwords.words("english")

def str_medidas(s):

    s = s.replace("'"," in. ") # character
    s = s.replace("inches"," in. ") # whole word
    s = s.replace("inch"," in. ") # whole word
    s = s.replace(" in "," in. ") # no period
    s = s.replace(" in."," in. ") # prefix space

    s = s.replace("''"," ft. ") # character
    s = s.replace(" feet "," ft. ") # whole word
    s = s.replace("feet"," ft. ") # whole word
    s = s.replace("foot"," ft. ") # whole word
    s = s.replace(" ft "," ft. ") # no period
    s = s.replace(" ft."," ft. ") # prefix space

    s = s.replace(" pounds "," lb. ") # character
    s = s.replace(" pound "," lb. ") # whole word
    s = s.replace("pound", " lb. ") # whole word
    s = s.replace(" lb "," lb. ") # no period
    s = s.replace(" lb."," lb. ")
    s = s.replace(" lbs "," lb. ")
    s = s.replace("lbs."," lb. ")

    s = s.replace("*"," xby ")
    s = s.replace(" by"," xby ")
    s = s.replace("x0"," xby 0 ")
    s = s.replace("x1"," xby 1 ")
    s = s.replace("x2"," xby 2 ")
    s = s.replace("x3"," xby 3 ")
    s = s.replace("x4"," xby 4 ")
    s = s.replace("x5"," xby 5 ")
    s = s.replace("x6"," xby 6 ")
    s = s.replace("x7"," xby 7 ")
    s = s.replace("x8"," xby 8 ")
    s = s.replace("x9"," xby 9 ")

    s = s.replace(" sq ft"," sq.ft. ")
    s = s.replace("sq ft"," sq.ft. ")
    s = s.replace("sqft"," sq.ft. ")
    s = s.replace(" sqft "," sq.ft. ")
    s = s.replace("sq. ft"," sq.ft. ")
    s = s.replace("sq ft."," sq.ft. ")
    s = s.replace("sq feet"," sq.ft. ")
    s = s.replace("square feet"," sq.ft. ")

    s = s.replace(" gallons "," gal. ") # character
    s = s.replace(" gallon "," gal. ") # whole word
    s = s.replace("gallons"," gal. ") # character
    s = s.replace("gallon"," gal. ") # whole word
    s = s.replace(" gal "," gal. ") # character
    s = s.replace(" gal"," gal. ") # whole word

    s = s.replace(" ounces"," oz. ")
    s = s.replace(" ounce"," oz. ")
    s = s.replace("ounce"," oz. ")
    s = s.replace(" oz "," oz. ")

    s = s.replace(" centimeters"," cm. ")
    s = s.replace(" cm."," cm. ")
    s = s.replace(" cm "," cm. ")

    s = s.replace(" milimeters"," mm. ")
    s = s.replace(" mm."," mm. ")
    s = s.replace(" mm "," mm. ")
    return s

def separateUpperCase(text):
    newtext = ''
    n = False
    for l in text:
        if l.isalpha():
            if l.isupper():
                if n:
                    n = False
                    newtext = newtext+ ' ' +l
                else:
                    n = False
                    newtext = newtext+l
            else :
                newtext = newtext + l
                n = True
        else:
            newtext = newtext + l
            n = False
    return newtext

def spaceNumber(text):
    newtext = ''
    n = False
    for l in text:
        if l in string.digits:
            if n:
                n = True
                newtext = newtext+l
            else:
                n = True
                newtext = newtext+' '+l
        else :
            if l in './p':
                newtext = newtext + l
            else :
                if n:
                    newtext = newtext +  ' '+l
                    n = False
                else:
                    newtext = newtext + l
                    n = False
    return newtext

def str_normalizar(s):
    s = separateUpperCase(s)
    s = spaceNumber(s)
    s = str_medidas(s)
    text = " ".join([word for word in s.split() if word not in cachedStopWords])
    return text



testx = '60 in. x 150 ft. 10/10 Remesh'
print testx

print str_normalizar(testx)

print separateUpperCase("Achieving delicious results is almost effortless with this Whirlpool over-the-range microwave hood with convection cooking. With 1.9 cu. ft. capacity, you'll have room to prepare multiple dishes at once. And, you won't have to micro-manage the cooking process thanks to the sensor cooking options. Sensor cooking tracks cooking progress and automatically adjusts cooking time. Convection roasting and baking allows you to bake cakes, bread, cookies and roast meats with ease. Clean-up is simple too with the industry leading CleanRelease non-stick interior surface. It easily releases cooked-on food without the use of harsh chemicals. A damp cloth or sponge is all that is needed to remove cooked-on spills and splashes.California residents: see&nbsp;Proposition 65 informationSpacious 1.9 cu. ft. capacity accommodates dinner plates and casserole dishes with ease1100 watts of cooking power and 10 cooking levels make cooking and reheating a snap400 CFM venting system whisks smoke, steam and odors away from the cooktop to keep your kitchen air clearSingle piece door with built-in touch-activated control console streamlines the exterior for a sleek, modern look and easy cleanupCook with confidence with the Sensor and Programmed cooking cycles and options. Sensor cycles include: Steam/Simmer, AccuPop and Potato for fast prep of family favoritesKids' Menu: it's simple, it's fast. The Kids' Menu is preset with cooking times and power levels for a variety of favorites like pizza and chicken nuggets. Now after school snacks don't have to be an afternoon hassleTimeSavor Plus True Convection cooking uses a 1600-watt element and a fan to circulate heat over, under and around food for fast cooking and even browningIndustry leading CleanRelease non-stick interior requires no special cleaners. A damp cloth or sponge is all that's needed to remove cooked-on spills")
