#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :

import time
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
from nltk.stem.porter import *
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression, chi2

import xgboost as xgb

strNum = {'zero':0, 'one':1, 'two':2, 'three':3, 'four':4, 'five':5, 'six':6, 'seven':7, 'eight':8, 'nine':9}

class DenseTransformer(BaseEstimator, TransformerMixin):

    def transform(self, X, y=None, **fit_params):
        return X.todense()

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y, **fit_params)
        return self.transform(X)

    def fit(self, X, y=None, **fit_params):
        return self
import HTMLParser
htmlParser = HTMLParser.HTMLParser()
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
def str_stem(s):

    if isinstance(s, basestring) or isinstance(s,str):
        # print s
        try:
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
            s = htmlParser.unescape(s)
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
        except UnicodeDecodeError:
            print '>>> OH shit'
            print type(s)
            print s
            # raise
            s = unicodedata.normalize('NFD', unicode(s.decode('utf-8'))).encode('ascii', 'ignore')
            s = htmlParser.unescape(s)
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
    else :
        s = str(s)
    s = htmlParser.unescape(s)
    s = re.sub(r"(\w)\.([A-Z])", r"\1 \2", s) #Split words with a.A
    s = s.lower()
    s = s.replace("  ", " ")
    s = s.replace(",", "") #could be number / segment later
    s = s.replace("$", " ")
    s = s.replace("?", " ")
    s = s.replace("-", " ")
    s = s.replace("//", "/")
    s = s.replace("..", ".")
    s = s.replace(" / ", " ")
    s = s.replace(" \\ ", " ")
    s = s.replace(".", " . ")
    s = re.sub(r"(^\.|/)", r"", s)
    s = re.sub(r"(\.|/)$", r"", s)
    s = re.sub(r"([0-9])([a-z])", r"\1 \2", s)
    s = re.sub(r"([a-z])([0-9])", r"\1 \2", s)
    s = s.replace(" x ", " xbi ")
    s = re.sub(r"([a-z])( *)\.( *)([a-z])", r"\1 \4", s)
    s = re.sub(r"([a-z])( *)/( *)([a-z])", r"\1 \4", s)
    s = s.replace("*", " xbi ")
    s = s.replace(" by ", " xbi ")
    s = re.sub(r"([0-9])( *)\.( *)([0-9])", r"\1.\4", s)
    s = re.sub(r"([0-9]+)( *)(inches|inch|in|')\.?", r"\1in. ", s)
    s = re.sub(r"([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1ft. ", s)
    s = re.sub(r"([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1lb. ", s)
    s = re.sub(r"([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1sq.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1cu.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1gal. ", s)
    s = re.sub(r"([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1oz. ", s)
    s = re.sub(r"([0-9]+)( *)(centimeters|cm)\.?", r"\1cm. ", s)
    s = re.sub(r"([0-9]+)( *)(milimeters|mm)\.?", r"\1mm. ", s)
    # s = s.replace(unicode("°"), " degrees ")
    s = re.sub(r"([0-9]+)( *)(degrees|degree)\.?", r"\1deg. ", s)
    s = s.replace(" v ", " volts ")
    s = re.sub(r"([0-9]+)( *)(volts|volt)\.?", r"\1volt. ", s)
    s = re.sub(r"([0-9]+)( *)(watts|watt)\.?", r"\1watt. ", s)
    s = re.sub(r"([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1amp. ", s)
    s = s.replace("  ", " ")
    s = s.replace(" . ", " ")
    s = (" ").join([str(strNum[z]) if z in strNum else z for z in s.split(" ")])
    s = (" ").join([stemmer.stem(z) for z in s.split(" ")])
    s = s.lower()
    s = s.replace("toliet", "toilet")
    s = s.replace("airconditioner", "air conditioner")
    s = s.replace("vinal", "vinyl")
    s = s.replace("vynal", "vinyl")
    s = s.replace("skill", "skil")
    s = s.replace("snowbl", "snow bl")
    s = s.replace("plexigla", "plexi gla")
    s = s.replace("rustoleum", "rust oleum")
    s = s.replace("whirpool", "whirlpool")
    s = s.replace("whirlpoolga", "whirlpool ga")
    s = s.replace("whirlpoolstainless", "whirlpool stainless")
    return s
    #else:
    #    return "null"

def seg_words(str1, str2):
    str2 = str2.lower()
    str2 = re.sub("[^a-z0-9./]", " ", str2)
    str2 = [z for z in set(str2.split()) if len(z) > 2]
    words = str1.lower().split(" ")
    s9 = []
    for word in words:
        if len(word) > 3:
            s1 = []
            s1 += segmentit(word, str2, True)
            if len(s9) > 1:
                s9 += [z for z in s1 if z not in ['er', 'ing', 's', 'less'] and len(z) > 1]
            else:
                s9.append(word)
        else:
            s9.append(word)
    return (" ".join(s9))

def segmentit(s, txt_arr, t):
    st = s
    r = []
    for j in xrange(len(s)):
        for word in txt_arr:
            if word == s[:-j]:
                r.append(s[:-j])
                #print(s[:-j],s[len(s)-j:])
                s = s[len(s)-j:]
                r += segmentit(s, txt_arr, False)
    if t:
        i = len(("").join(r))
        if not i == len(st):
            r.append(st[i:])
    return r

def str_common_word(str1, str2):
    words, cnt = str1.split(), 0
    for word in words:
        if str2.find(word) >= 0:
            cnt += 1
    return cnt

def str_common_word_freq(str1, str2, freqs):
    words, cnt = str1.split(), 0
    for word in words:
        if str2.find(word) >= 0:
            if word in freqs:
                cnt += 1 * freqs[word]
            else:
                cnt += 1
    return cnt

def str_whole_word(str1, str2, i_):
    cnt = 0
    while i_ < len(str2):
        i_ = str2.find(str1, i_)
        if i_ == -1:
            return cnt
        else:
            cnt += 1
            i_ += len(str1)
    return cnt

def fmean_squared_error(ground_truth, predictions):
    fmean_squared_error_ = mean_squared_error(ground_truth, predictions)**0.5
    return fmean_squared_error_

RMSE = make_scorer(fmean_squared_error, greater_is_better=False)

unnecessari_fields = ['id', 'relevance', 'search_term', 'product_title', 'product_description', 'product_info', 'attr', 'brand','attsall','attrs','product_info_attrs',
    "alltogether", "product_alltogethers", 'bullets', 'sizes', 'thingsYes', 'thingsNo', 'prod_acts']

class cust_regression_vals(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, hd_searches):
        d_col_drops = unnecessari_fields
        hd_searches = hd_searches.drop(d_col_drops, axis=1).values
        return hd_searches

class cust_txt_col(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        return data_dict[self.key].apply(str)

def celll(n):
    v = float(n)
    if v > 3.0:
        return str(3)
    return str(v)

if __name__ == '__main__':
    df_train = pd.read_csv('../input/trainSpellCheck.csv', encoding="ISO-8859-1")#[:1000] #update here
    df_test = pd.read_csv('../input/testSpellCheck.csv', encoding="ISO-8859-1")#[:1000] #update here
    df_pro_desc = pd.read_csv('../input/product_descriptions.csv')#[:1000] #update here
    df_attrs = pd.read_csv('../input/pau-atts-separate.csv')#[:1000] #update here
    df_attr2 = pd.read_csv('../input/pau-atts2.csv')#[:1000] #update here


    df_attr = pd.read_csv('../input/attributes.csv')
    df_brand = df_attr[df_attr["name"] == "MFG Brand Name"][["product_uid", "value"]].rename(columns={"value": "brand"})
    num_train = df_train.shape[0]
    df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)
    df_all = pd.merge(df_all, df_pro_desc, how='left', on='product_uid')
    df_all = pd.merge(df_all, df_brand, how='left', on='product_uid')
    df_all = pd.merge(df_all, df_attrs, how='left', on='product_uid')
    df_all = pd.merge(df_all, df_attr2, how='left', on='product_uid')
    print("--- Files Loaded: %s minutes ---" % round(((time.time() - start_time)/60), 2))

    df_all = pd.read_csv('df_all.csv', encoding="ISO-8859-1", index_col=0)

    # df_all = pd.merge(df_all, df_attrs[['product_uid','theRest']], how='left', on='product_uid')
    # print df_all

    # df_all['bullets'] = df_all['bullets'].fillna('')
    # df_all['thingsYes'] = df_all['thingsYes'].fillna('')
    # df_all['thingsNo'] = df_all['thingsNo'].fillna('')
    # df_all['sizes'] = df_all['sizes'].fillna('')
    # df_all['theRest'] = df_all['theRest'].fillna('')

    #comment out the lines below use df_all.csv for further grid search testing
    #if adding features consider any drops on the 'cust_regression_vals' class
    #*** would be nice to have a file reuse option or script chaining option on Kaggle Scripts ***

    # df_all['search_term'] = df_all['search_term'].map(lambda x: str_stem(x))
    # df_all['product_title'] = df_all['product_title'].map(lambda x: str_stem(x))
    # df_all['product_description'] = df_all['product_description'].map(lambda x: str_stem(x))
    # df_all['bullets'] = df_all['bullets'].map(lambda x: str_stem(x))
    # df_all['thingsYes'] = df_all['thingsYes'].map(lambda x: str_stem(x))
    # df_all['thingsNo'] = df_all['thingsNo'].map(lambda x: str_stem(x))
    # df_all['theRest'] = df_all['theRest'].map(lambda x: str_stem(x))
    #
    #
    #
    # corpus = df_all['product_description']
    # vectorizer = TfidfVectorizer(min_df=1)
    # X = vectorizer.fit_transform(corpus)
    # idf = vectorizer.idf_
    # product_description_dict = dict(zip(vectorizer.get_feature_names(), idf))
    #
    #
    # df_all['brand'] = df_all['brand'].map(lambda x: str_stem(x))
    # print("--- Stemming: %s minutes ---" % round(((time.time() - start_time)/60), 2))
    # df_all['product_info'] = df_all['search_term']+"|"+df_all['product_title'] +"|"+df_all['product_description']
    # print("--- Prod Info: %s minutes ---" % round(((time.time() - start_time)/60), 2))
    # df_all['len_of_query'] = df_all['search_term'].map(lambda x: len(x.split())).astype(int)
    #

    # df_all['attsall'] = df_all['attrs'].map(lambda x: str_stem(x))
    # df_all['len_of_attsall'] = df_all['attsall'].map(lambda x: len(x.split())).astype(int)
    # df_all['product_info_attrs'] = df_all['search_term']+"|"+df_all['attsall']
    # df_all['query_in_attrs'] = df_all['product_info_attrs'].map(lambda x: str_whole_word(x.split('|')[0], x.split('|')[1], 0))
    # df_all['ratio_word_in_attrs'] = df_all['query_in_attrs']/df_all['len_of_query']
    # df_all['word_in_attrs_freq'] = df_all['product_info_attrs'].map(lambda x: str_common_word_freq(x.split('|')[0], x.split('|')[1], product_description_dict))
    # df_all['ratio_word_in_attrs_freq'] = df_all['word_in_attrs_freq']/df_all['len_of_query']
    # df_all['query_last_word_in_attrs'] = df_all['product_info_attrs'].map(lambda x: str_common_word(x.split('|')[0].split(" ")[-1], x.split('|')[1]))
    #
    #

    #
    # df_all['prod_acts'] = df_all['search_term'] +"|"+ df_all['bullets'] +"|"+ df_all['thingsYes'] +"|"+ df_all['thingsNo'] +"|"+ df_all['sizes'] +"|"+ df_all['theRest']
    #
    # df_all['query_words_bullets'] = df_all['prod_acts'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[1]))
    # df_all['query_words_bullets_freq'] = df_all['prod_acts'].map(lambda x: str_common_word_freq(x.split('|')[0], x.split('|')[1], product_description_dict))
    # df_all['ratio_word_bullets'] = df_all['query_words_bullets']/df_all['len_of_query']
    # df_all['ratio_word_bullets_freq'] = df_all['query_words_bullets_freq']/df_all['len_of_query']
    # df_all['query_words_thingsYes'] = df_all['prod_acts'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[2]))
    # df_all['query_words_thingsNo'] = df_all['prod_acts'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[3]))
    # df_all['query_words_sizes'] = df_all['prod_acts'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[4]))
    # df_all['query_words_therest'] = df_all['prod_acts'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[5]))

    #
    #
    #
    # df_all['len_of_title'] = df_all['product_title'].map(lambda x: len(x.split())).astype(int)
    # df_all['len_of_description'] = df_all['product_description'].map(lambda x: len(x.split())).astype(int)
    # df_all['len_of_brand'] = df_all['brand'].map(lambda x: len(x.split())).astype(int)
    # print("--- Len of: %s minutes ---" % round(((time.time() - start_time)/60), 2))
    # df_all['search_term'] = df_all['product_info'].map(lambda x: seg_words(x.split('|')[0], x.split('|')[1]))
    # #print("--- Search Term Segment: %s minutes ---" % round(((time.time() - start_time)/60),2))
    # df_all['query_in_title'] = df_all['product_info'].map(lambda x: str_whole_word(x.split('|')[0], x.split('|')[1], 0))
    # df_all['query_in_description'] = df_all['product_info'].map(lambda x: str_whole_word(x.split('|')[0], x.split('|')[2], 0))
    # print("--- Query In: %s minutes ---" % round(((time.time() - start_time)/60), 2))
    # df_all['query_last_word_in_title'] = df_all['product_info'].map(lambda x: str_common_word(x.split('|')[0].split(" ")[-1], x.split('|')[1]))
    # df_all['query_last_word_in_description'] = df_all['product_info'].map(lambda x: str_common_word(x.split('|')[0].split(" ")[-1], x.split('|')[2]))
    # print("--- Query Last Word In: %s minutes ---" % round(((time.time() - start_time)/60), 2))
    # df_all['word_in_title'] = df_all['product_info'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[1]))
    # df_all['word_in_description'] = df_all['product_info'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[2]))
    #
    # df_all['word_in_title_freq'] = df_all['product_info'].map(lambda x: str_common_word_freq(x.split('|')[0], x.split('|')[1], product_description_dict))
    # df_all['word_in_description_freq'] = df_all['product_info'].map(lambda x: str_common_word_freq(x.split('|')[0], x.split('|')[2], product_description_dict))
    # df_all['ratio_word_in_title_freq'] = df_all['word_in_title_freq']/df_all['len_of_query']
    # df_all['ratio_word_in_description_freq'] = df_all['word_in_description_freq']/df_all['len_of_query']
    #
    # df_all['ratio_title'] = df_all['word_in_title']/df_all['len_of_query']
    # df_all['ratio_description'] = df_all['word_in_description']/df_all['len_of_query']
    # df_all['attr'] = df_all['search_term']+"|"+df_all['brand']
    # df_all['word_in_brand'] = df_all['attr'].map(lambda x: str_common_word(x.split('|')[0], x.split('|')[1]))
    # df_all['ratio_brand'] = df_all['word_in_brand']/df_all['len_of_brand']
    #
    # df_all['alltogether'] = df_all['product_title'] + " " + df_all['product_description'] + " " + df_all['attsall']
    # df_all['len_of_attsall'] = df_all['alltogether'].map(lambda x: len(x.split())).astype(int)
    # df_all['product_alltogethers'] = df_all['search_term']+"|"+df_all['alltogether']
    # df_all['query_in_alltogether'] = df_all['product_alltogethers'].map(lambda x: str_whole_word(x.split('|')[0], x.split('|')[1], 0))
    # df_all['ratio_word_in_alltogether'] = df_all['query_in_alltogether']/df_all['len_of_query']
    # df_all['word_in_alltogether_freq'] = df_all['product_alltogethers'].map(lambda x: str_common_word_freq(x.split('|')[0], x.split('|')[1], product_description_dict))
    # df_all['ratio_word_in_attrs_freq'] = df_all['word_in_alltogether_freq']/df_all['len_of_query']
    # df_all['query_last_word_in_alltogether'] = df_all['product_alltogethers'].map(lambda x: str_common_word(x.split('|')[0].split(" ")[-1], x.split('|')[1]))
    #
    # df_brand = pd.unique(df_all.brand.ravel())
    # d = {}
    # i = 1000
    # for s8 in df_brand:
    #     d[s8] = i
    #     i += 3
    # df_all['brand_feature'] = df_all['brand'].map(lambda x: d[x])
    # df_all['search_term_feature'] = df_all['search_term'].map(lambda x: len(x))
    # df_all.to_csv('df_all.csv')
    # df_all = pd.read_csv('df_all.csv', encoding="ISO-8859-1", index_col=0)
    df_train = df_all.iloc[:num_train]
    df_test = df_all.iloc[num_train:]
    id_test = df_test['id']
    y_train = df_train['relevance'].values
    X_train = df_train[:]
    X_test = df_test[:]
    print("--- Features Set: %s minutes ---" % round(((time.time() - start_time)/60), 2))

    unnecessari_fields = ['id', 'relevance', 'search_term', 'product_title', 'product_description', 'product_info', 'attr', 'brand','attsall','attrs','product_info_attrs',
        "alltogether", "product_alltogethers", 'bullets', 'sizes', 'thingsYes', 'thingsNo', 'prod_acts', 'len_of_attsall', 'theRest'
        # ,'query_in_alltogether', 'ratio_word_in_alltogether', 'ratio_word_in_alltogether', 'word_in_alltogether_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_alltogether'
        # ,'query_in_attrs', 'ratio_word_in_attrs', 'word_in_attrs_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_attrs'
        ]



    # print X_train
    rfr = GradientBoostingRegressor(n_estimators=25, random_state=2016, verbose=5)
    tfidf = TfidfVectorizer(ngram_range=(1, 1), stop_words='english')
    tsvd = TruncatedSVD(n_components=10, random_state=2016)

    kbest = SelectKBest(f_regression)

    gbm = xgb.XGBRegressor()

    # tsvd = RandomizedPCA(n_components=10, random_state=2016)

    clf = Pipeline([
        ('union', FeatureUnion(transformer_list=[
            ('cst', cust_regression_vals()),
            ('txt1', Pipeline([
                ('s1', cust_txt_col(key='search_term')),
                ('tfidf1', tfidf)


                # ,('tsvd1', tsvd)
            ]))])),

        # ('kbest', kbest),
        ('model', gbm)])
        # ('rfr', rfr)])

    # param_grid = {'rfr__max_features': [10], 'rfr__max_depth': [20]}

    param_grid = {
        'model__n_estimators': [1000],
        'model__max_depth': [7],
        'model__learning_rate': [0.05],
        'model__base_score': [0.9]
        # 'rfr__n_estimators': [300, 200, 100, 50 , 20]
    }

    # X_train = X_train.drop(unnecessari_fields, axis=1)
    # X_test = X_test.drop(unnecessari_fields, axis=1)
    # hd_searches = hd_searches.drop(d_col_drops, axis=1)

    model = grid_search.GridSearchCV(estimator = clf, param_grid = param_grid, n_jobs = -1, cv = 2, verbose = 20, scoring=RMSE)
    model.fit(X_train, y_train)

    print("Best parameters found by grid search:")
    print(model.best_params_)
    print("Best CV score:")
    print(model.best_score_)
    print(model.best_score_ + 0.47003199274)

    y_pred = model.predict(X_test)
    pp = pd.DataFrame({"id": id_test, "relevance": y_pred})

    # print pd['relevance']
    pp['relevance'] = pp['relevance'].map(lambda x: celll(x))

    pp.to_csv('submission5.csv', index=False)
    print("--- Training & Testing: %s minutes ---" % round(((time.time() - start_time)/60), 2))
