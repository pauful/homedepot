#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import json
import re

csvfile = open('../input/attributes.csv', 'r')

listP = {}

fieldnames = ("product_uid","name","value")
reader = csv.DictReader( csvfile, fieldnames)
i = 0
hh = 0
for row in reader:

    try:
        if row['product_uid'] not in listP:

            listP[row['product_uid']] = {
                'product_uid': int(row['product_uid']),
                'attrs': ''
            }
        if row['value'] != 'Yes' and row['value'] != 'No' and row['name'] != 'Bullet22' and row['name'] != 'Bullet20' and row['value'] != 'Note: product may vary by store':
            listP[row['product_uid']]['attrs'] = listP[row['product_uid']]['attrs'] + " " + row['value']

        hh += 1
    except ValueError as e:
        i += 1
        print 'hh'+row['product_uid']+'hh' + str(i) + '/' + str(hh)




print list(listP.values())[0]

with open('../input/pau-attributes-all.csv', 'wb') as output_file:
    dict_writer = csv.DictWriter(output_file, ['product_uid', 'attrs'])
    dict_writer.writeheader()
    dict_writer.writerows(list(listP.values()))
