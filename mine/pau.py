# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
import string
import re
from nltk.corpus import stopwords
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import wordnet as wn
from sets import Set
import enchant
import sys

nltk.download('stopwords')
d = enchant.Dict("en_US")
cachedStopWords = stopwords.words("english")

stemmer = SnowballStemmer('english')

df_train = pd.read_csv('train.csv', encoding="ISO-8859-1")
df_test = pd.read_csv('test.csv', encoding="ISO-8859-1")
# df_attr = pd.read_csv('../input/attributes.csv')
df_pro_desc = pd.read_csv('product_descriptions.csv')

from sklearn.feature_extraction.text import TfidfVectorizer
corpus = df_pro_desc['product_description']
vectorizer = TfidfVectorizer(min_df=1)
X = vectorizer.fit_transform(corpus)
idf = vectorizer.idf_
product_description_dict = dict(zip(vectorizer.get_feature_names(), idf))

df_attr = pd.read_csv('pau-atts2.csv')

# df_attr_grouped = df_attr.groupby('product_uid')['value'].agg(lambda x:''.join(str(x)))
# df_attr_grouped = df_attr.groupby('product_uid')['value'].agg(lambda x:','.join(str(x)))

# df_attr_grouped.to_csv('pau-atts.csv')
#
# print df_attr_grouped.iloc[:30]


num_train = df_train.shape[0]

def findNumWordsWithSyms(text1, text2):
    s = 0
    # print '///////////////'
    # print text1
    # print text2
    for word1 in text1.split():
        # print word1
        if word1.isdigit() == False:

            for word2 in text2.split():
                # print word2.isdigit
                if word2.isdigit() == False:
                    # print 'in'
                    a = []

                    for ss in wn.synsets(word2):
                        newLemmas = [stemmer.stem(lema) for lema in ss.lemma_names()]
                        a = a + newLemmas + ss.lemma_names()
                    a.append(word2)
                    if word1 in a:
                        # print word1 + ' in ' + str(a)
                        s += 1
        if word1.isdigit() == False:
            # print 'digit'
            if word1 in text2.split():
                # print 'OK'
                s += 1

    return s

from bs4 import BeautifulSoup
def description_data_cleansing(s):
    s = BeautifulSoup(s, 'html.parser')
    s = s.replace("  "," ")
    s = s.replace("*"," ")
    s = s.replace("?"," ")
    s = s.replace("$"," ")
    s = s.replace("-"," ")
    s = s.replace("-"," ")
    s = re.sub(r"([a-z])([A-Z]+[a-z]*)", r"\1 \2", s)

    s = re.sub(r"([^0-9]+)\.([^0-9]+)", r"\1 \2", s)
    s = re.sub(r"([0-9]+)\.([^0-9]+)", r"\1 \2", s)

    s = re.sub(r"([0-9]+),([0-9]+)", r"\1.\2", s)

    s = s.replace(","," ")

    return s


def fix_metrics(s):
    s = re.sub(r"([0-9]+)( *)(inches|inch|in|')\.?", r"\1 in. ", s)
    s = re.sub(r"([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1 ft. ", s)
    s = re.sub(r"([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1 lb. ", s)
    s = re.sub(r"([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1 sq.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1 cu.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1 gal. ", s)
    s = re.sub(r"([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1 oz. ", s)
    s = re.sub(r"([0-9]+)( *)(centimeters|cm)\.?", r"\1 cm. ", s)
    s = re.sub(r"([0-9]+)( *)(milimeters|mm)\.?", r"\1 mm. ", s)
    s = s.replace("°"," degrees ")
    s = re.sub(r"([0-9]+)( *)(degrees|degree)\.?", r"\1 deg. ", s)
    s = re.sub(r"([0-9]+)( *)(v)\.?", r"\1 volts. ", s)
    s = re.sub(r"([0-9]+)( *)(volts|volt)\.?", r"\1 volt. ", s)
    s = re.sub(r"([0-9]+)( *)(watts|watt)\.?", r"\1 watt. ", s)
    s = re.sub(r"([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1 amp. ", s)

    return s

def spell_check(s):
    s = s.replace("toliet","toilet")
    s = s.replace("airconditioner","air conditioner")
    s = s.replace("vinal","vinyl")
    s = s.replace("vynal","vinyl")
    s = s.replace("skill","skil")
    s = s.replace("snowbl","snow bl")
    s = s.replace("plexigla","plexi gla")
    s = s.replace("rustoleum","rust-oleum")
    s = s.replace("whirpool","whirlpool")
    s = s.replace("whirlpoolga", "whirlpool ga")
    s = s.replace("whirlpoolstainless","whirlpool stainless")
    s = s.replace("cedart ","cedar ")
    s = s.replace("ceadar ","cedar ")
    s = s.replace("celing ","ceiling ")
    s = s.replace("cieling ","ceiling ")
    s = s.replace("aspiradora ","vacuum cleaner ")
    s = s.replace("vaccum ","vacuum ")
    s = s.replace("kombo ","combo ")
    s = s.replace("windos ","windows ")
    s = s.replace("softners ","softeners ")
    s = s.replace("pedistal ","pedestal ")
    s = s.replace("porcelin ","porcelain ceramic ")
    s = s.replace("flourescent ","fluorescent ")
    s = s.replace("pannels ","panels ")
    s = s.replace("gridlee ","griddle ")
    s = s.replace("berh ","behr ")
    s = s.replace("heaterconditioner ","air conditioner ")
    s = s.replace("steele ","steel ")

    steele


    s = s.replace("frontload ","front load ")





    return s

def str_common_word(str1, str2):
	return sum(int(str2.find(word)>=0) for word in str1.split())

def str_common_word_desc(str1, str2, d):
	return sum(int(str2.find(word)>=0) * d[word] for word in str1.split() if word in d)

def checkStemWordInText(word, text):
    text = " ".join([stemmer.stem(word) for word in text.lower().split() if word not in cachedStopWords])
    if(stemmer.stem(word) in text):
        return True
    return False


def spell_check_stem(str1, str2):
    # print '++++++++++++'


    str1 = str1.lower()
    str1 = re.sub('[%s]' % string.punctuation,' ',str1)
    str1 = spaceNumber(str1)


    str2 = str2.lower()
    str2 = re.sub('[%s]' % string.punctuation,' ',str2)
    str2 = spaceNumber(str2)

    str2Stemmed = " ".join([stemmer.stem(word) for word in str2.lower().split() if word not in cachedStopWords])

    newText = ''
    # print str1
    # print str2
    for w in str1.split():
        # print '>>>>>>>>>>>>>>>>'
        # print w
        if d.check(w) != True:
            # print 'not valid'
            stemmed = stemmer.stem(w)
            ss = [k for k in str2Stemmed.split() if k == stemmed]
            tt = [t for t in str2.split() if t == w]
            if len(ss) > 0 or len(tt) > 0:
                newText += newText + ' ' + w
            else:
                listM = d.suggest(w)
                if len(listM) > 0:
                    # print w + ' = ' + listM[0]
                    newText += newText + ' ' + listM[0]
                else:
                    # print w + ' = ' + stemmer.stem(w)
                    newText += newText + ' ' + w
        else:
            # print 'valid'
            # print w
            # print stemmer.stem(w)
            # print 'vvvvvvv'
            newText += ' ' + w
        # print newText
        # print '.....'
    # print '~~~~~~~~~~~~~~~~~~~~~'
    # print newText

    return newText

print 'ay'
df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)

df_all = pd.merge(df_all, df_pro_desc, how='left', on='product_uid')

# print pd.merge(df_all, df_attr_grouped, how='left', on='product_uid')

# print df_all.iloc[:3]
df_attr['product_uid'].apply(float)

# df_all = df_all.join(df_attr, on='product_uid')
df_all = pd.merge(df_all, df_attr, how='left', on='product_uid')
# df_all = pd.merge(df_all, df_attr, how='left', on='product_uid')
# print df_all['value'].iloc[:300]
print "//////--------------------~~~~~~~~~~~~~~~~~---------------"
# df_all['attributes'] = df_all.groupby('product_uid')['value'].agg(lambda x:''.join(str(x)))
#
# print df_all.iloc[:30]

# df_all['search_att'] = df_all['attributes'] + '\t' + df_all['search_term']

print 'Normal columns'

# df_all['attrs'] = df_all['attrs'].map(lambda x:str(x).decode("utf8"))
# df_all['product_all_info'] = df_all['product_title']+" "+df_all['product_description']+ " " +df_all['attrs']
# df_all['search_att'] = df_all['search_term']+"\t"+df_all['attrs']
# df_all['search_and_all_info'] = df_all['search_term'] +"\t"+df_all['product_all_info']
#
#
# print 'Calcular search spell'
#
# df_all['search_spell_check'] = df_all['search_and_all_info'].map(lambda x:spell_check_stem(x.split('\t')[0],x.split('\t')[1]))
#
#
# # df_all.to_csv('dataTocheck.csv',index=False, encoding='utf-8')
#
# # sys.exit("Error message")
#
# df_all['attrs_stem'] = df_all['attrs'].map(lambda x:str_stemmer(x))
#
# print 'Stem de search spell'
#
# df_all['search_spell_check_stem'] = df_all['search_spell_check'].map(lambda x:str_stemmer(x))
#
# print 'Title y description stem'
#
# df_all['product_title_stem'] = df_all['product_title'].map(lambda x:str_stemmer(x))
# df_all['product_description_stem'] = df_all['product_description'].map(lambda x:str_stemmer(x))
# joblib.dump(df_all, 'df_all4.txt')
# print 'Spell search + title spem + desc stem '
#
# df_all['product_info_spell'] = df_all['search_spell_check_stem']+"\t"+df_all['product_title_stem']+"\t"+df_all['product_description_stem']
#
# print 'Calcular similar words en search spell y title stem o desc stem'
#
# df_all['word_in_title_spell'] = df_all['product_info_spell'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
# df_all['word_in_description_spell'] = df_all['product_info_spell'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[2]))
#
# print 'Stem atts'
# joblib.dump(df_all, 'df_all4.txt')
#
# df_all['search_att_spell'] = df_all['search_spell_check_stem'] +"\t"+df_all['attrs_stem']
#
# print 'Calcular spell and words in atts'
# df_all['word_in_attributes_spell'] = df_all['search_att_spell'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
# joblib.dump(df_all, 'df_all4.txt')
# print 'Stem search term'
#
# df_all['search_term_stem'] = df_all['search_term'].map(lambda x:str_stemmer(x))
#
# print 'Unir search stem con attrs stemmed'
# joblib.dump(df_all, 'df_all4.txt')
# df_all['search_att_stem'] = df_all['search_term_stem']+"\t"+df_all['attrs_stem']
#
# print 'Find length of query'
# df_all['len_of_query'] = df_all['search_term'].map(lambda x:len(x.split())).astype(np.int64)
#
# print 'Campos only stem'
# df_all['product_info_stem'] = df_all['search_term_stem']+"\t"+df_all['product_title_stem']+"\t"+df_all['product_description_stem']
#
#
# print 'TfIdf Stem search + stem desc'
# df_all['word_in_description_tf_stem'] = df_all['product_info_stem'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[2], product_description_dict))
#
# print 'TfIdf Spell search + stem desc'
# df_all['word_in_description_tf_stem'] = df_all['product_info_spell'].map(lambda x:str_common_word_desc(x.split('\t')[0],x.split('\t')[2], product_description_dict))
# joblib.dump(df_all, 'df_all4.txt')

df_all = joblib.load('df_all4.txt')

print 'Calcular words en stem search + title o desc'
df_all['word_in_title_stem'] = df_all['product_info_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))
df_all['word_in_description_stem'] = df_all['product_info_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[2]))
print 'Calcular words en stem search + atts'
df_all['word_in_attributes_stem'] = df_all['search_att_stem'].map(lambda x:str_common_word(x.split('\t')[0],x.split('\t')[1]))

print 'Syms for stemming'

df_all['word_with_synonims_desc'] = df_all['product_info_spell'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[2]))
df_all['word_with_synonims_title'] = df_all['product_info_spell'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
df_all['word_with_synonims_att'] = df_all['search_att_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))

joblib.dump(df_all, 'df_all4.txt')

print 'Syms for spell'

df_all['word_with_synonims_desc_spell'] = df_all['product_info_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[2]))
df_all['word_with_synonims_title_spell'] = df_all['product_info_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))
df_all['word_with_synonims_att_spell'] = df_all['search_spell_check_stem'].map(lambda x:findNumWordsWithSyms(x.split('\t')[0],x.split('\t')[1]))


print df_all.iloc[:3]

joblib.dump(df_all, 'df_all4.txt')


df_all = df_all.drop(['search_term','product_title','product_description','product_info', 'search_att', 'attrs',
    'search_term_stem','product_title_stem','product_description_stem','product_info_stem', 'search_att_stem', 'attrs_stem',
    'product_all_info','search_att_spell','product_info_spell','search_spell_check','search_and_all_info'],axis=1)
print 'ay'
df_train = df_all.iloc[:num_train]
df_test = df_all.iloc[num_train:]
id_test = df_test['id']

y_train = df_train['relevance'].values
X_train = df_train.drop(['id','relevance'],axis=1).values
X_test = df_test.drop(['id','relevance'],axis=1).values
print 'ay'
rf = RandomForestRegressor(n_estimators=15, max_depth=6, random_state=0)
clf = BaggingRegressor(rf, n_estimators=45, max_samples=0.1, random_state=25)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print 'ay'
pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
print 'ay'
