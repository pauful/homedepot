#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :

import time
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
from nltk.stem.porter import *
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression

import xgboost as xgb

df_s1 = pd.read_csv('best/submission14.csv', encoding="ISO-8859-1")#[:1000] #update here
df_s2 = pd.read_csv('best/submission16.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s3 = pd.read_csv('best/ksubmission3.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s4 = pd.read_csv('best/ksubmission4.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s5 = pd.read_csv('best/ksubmission5.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s6 = pd.read_csv('best/ksubmission6.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s7 = pd.read_csv('best/ksubmission7.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s8 = pd.read_csv('best/ksubmission8.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s9 = pd.read_csv('best/ksubmission9.csv', encoding="ISO-8859-1")#[:1000] #update here
# df_s10 = pd.read_csv('best/ksubmission10.csv', encoding="ISO-8859-1")#[:1000] #update here

df_s1['relevance'] = (df_s1['relevance'] + df_s2['relevance']) / 2

print df_s1['relevance']
print df_s2['relevance']

def celll(n):
    v = float(n)
    if v > 3.0:
        print 'BIIIg = ' + str(v)
        return str(3)
    return str(v)

# df_all = pd.merge(df_all, df_brand, how='left', on='product_uid')

# pp = pd.DataFrame({"id": id_test, "relevance": y_pred})
#
# # print pd['relevance']
# pp['relevance'] = pp['relevance'].map(lambda x: celll(x))
#
df_s1['relevance'] = df_s1['relevance'].map(lambda x: celll(x))

df_s1.to_csv('submissionSum.csv', index=False)
# print("--- Training & Testing: %s minutes ---" % round(((time.time() - start_time)/60), 2))
