#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :

import time
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
from nltk.stem.porter import *
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata



def sub(s):
    s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')

    while( re.match(r'.*([a-z][A-Z]).*', s)):

            s = s.replace(re.match(r'.*([a-z][A-Z]).*', s).group(1),' ')


    while( re.match(r'.*([a-z][0-9]).*', s)):
            # print re.match(r'.*([a-zA-Z][0-9]).*', s).group(1)
            s = s.replace(re.match(r'.*([a-z][0-9]).*', s).group(1),' ')


    # while( re.match(r'.*([A-Z][0-9]).*', s)):
    #         print s
    #         print re.match(r'.*([A-Z][0-9]).*', s).group(1)
    #         quit()
    #         s = s.replace(re.match(r'.*([A-Z][0-9]).*', s).group(1),' ')

    return s

if __name__ == '__main__':
    df_train = pd.read_csv('../input/product_descriptions.csv', encoding="ISO-8859-1")#[:1000] #update here
    # df_test = pd.read_csv('../input/test.csv', encoding="ISO-8859-1")#[:1000] #update here

    df_train['product_description'] = df_train['product_description'].map(lambda x: sub(x))
    # df_test['search_term'] = df_test['search_term'].map(lambda x: sub(x))

    df_train.to_csv('product_descriptions_correct.csv', index=False, encoding='utf-8')
    # df_test.to_csv('testSpellCheck.csv', index=False, encoding='utf-8')
