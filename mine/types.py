#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :

import time
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
from nltk.stem.porter import *
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression, chi2

import xgboost as xgb

strNum = {'zero':0, 'one':1, 'two':2, 'three':3, 'four':4, 'five':5, 'six':6, 'seven':7, 'eight':8, 'nine':9}

class DenseTransformer(BaseEstimator, TransformerMixin):

    def transform(self, X, y=None, **fit_params):
        return X.todense()

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y, **fit_params)
        return self.transform(X)

    def fit(self, X, y=None, **fit_params):
        return self
import HTMLParser
htmlParser = HTMLParser.HTMLParser()
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
def str_stem(s):

    if isinstance(s, basestring) or isinstance(s,str):
        # print s
        try:
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
            s = htmlParser.unescape(s)
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
        except UnicodeDecodeError:
            print '>>> OH shit'
            print type(s)
            print s
            # raise
            s = unicodedata.normalize('NFD', unicode(s.decode('utf-8'))).encode('ascii', 'ignore')
            s = htmlParser.unescape(s)
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
    else :
        s = str(s)
    s = htmlParser.unescape(s)
    s = re.sub(r"(\w)\.([A-Z])", r"\1 \2", s) #Split words with a.A
    s = s.lower()
    s = s.replace("  ", " ")
    s = s.replace(",", "") #could be number / segment later
    s = s.replace("$", " ")
    s = s.replace("?", " ")
    s = s.replace("-", " ")
    s = s.replace("//", "/")
    s = s.replace("..", ".")
    s = s.replace(" / ", " ")
    s = s.replace(" \\ ", " ")
    s = s.replace(".", " . ")
    s = re.sub(r"(^\.|/)", r"", s)
    s = re.sub(r"(\.|/)$", r"", s)
    s = re.sub(r"([0-9])([a-z])", r"\1 \2", s)
    s = re.sub(r"([a-z])([0-9])", r"\1 \2", s)
    s = s.replace(" x ", " xbi ")
    s = re.sub(r"([a-z])( *)\.( *)([a-z])", r"\1 \4", s)
    s = re.sub(r"([a-z])( *)/( *)([a-z])", r"\1 \4", s)
    s = s.replace("*", " xbi ")
    s = s.replace(" by ", " xbi ")
    s = re.sub(r"([0-9])( *)\.( *)([0-9])", r"\1.\4", s)
    s = re.sub(r"([0-9]+)( *)(inches|inch|in|')\.?", r"\1in. ", s)
    s = re.sub(r"([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1ft. ", s)
    s = re.sub(r"([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1lb. ", s)
    s = re.sub(r"([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1sq.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1cu.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1gal. ", s)
    s = re.sub(r"([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1oz. ", s)
    s = re.sub(r"([0-9]+)( *)(centimeters|cm)\.?", r"\1cm. ", s)
    s = re.sub(r"([0-9]+)( *)(milimeters|mm)\.?", r"\1mm. ", s)
    # s = s.replace(unicode("°"), " degrees ")
    s = re.sub(r"([0-9]+)( *)(degrees|degree)\.?", r"\1deg. ", s)
    s = s.replace(" v ", " volts ")
    s = re.sub(r"([0-9]+)( *)(volts|volt)\.?", r"\1volt. ", s)
    s = re.sub(r"([0-9]+)( *)(watts|watt)\.?", r"\1watt. ", s)
    s = re.sub(r"([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1amp. ", s)
    s = s.replace("  ", " ")
    s = s.replace(" . ", " ")
    s = (" ").join([str(strNum[z]) if z in strNum else z for z in s.split(" ")])
    s = (" ").join([stemmer.stem(z) for z in s.split(" ")])
    s = s.lower()
    s = s.replace("toliet", "toilet")
    s = s.replace("airconditioner", "air conditioner")
    s = s.replace("vinal", "vinyl")
    s = s.replace("vynal", "vinyl")
    s = s.replace("skill", "skil")
    s = s.replace("snowbl", "snow bl")
    s = s.replace("plexigla", "plexi gla")
    s = s.replace("rustoleum", "rust oleum")
    s = s.replace("whirpool", "whirlpool")
    s = s.replace("whirlpoolga", "whirlpool ga")
    s = s.replace("whirlpoolstainless", "whirlpool stainless")
    return s
    #else:
    #    return "null"

def seg_words(str1, str2):
    str2 = str2.lower()
    str2 = re.sub("[^a-z0-9./]", " ", str2)
    str2 = [z for z in set(str2.split()) if len(z) > 2]
    words = str1.lower().split(" ")
    s9 = []
    for word in words:
        if len(word) > 3:
            s1 = []
            s1 += segmentit(word, str2, True)
            if len(s9) > 1:
                s9 += [z for z in s1 if z not in ['er', 'ing', 's', 'less'] and len(z) > 1]
            else:
                s9.append(word)
        else:
            s9.append(word)
    return (" ".join(s9))

def segmentit(s, txt_arr, t):
    st = s
    r = []
    for j in xrange(len(s)):
        for word in txt_arr:
            if word == s[:-j]:
                r.append(s[:-j])
                #print(s[:-j],s[len(s)-j:])
                s = s[len(s)-j:]
                r += segmentit(s, txt_arr, False)
    if t:
        i = len(("").join(r))
        if not i == len(st):
            r.append(st[i:])
    return r

def str_common_word(str1, str2):
    words, cnt = str1.split(), 0
    for word in words:
        if str2.find(word) >= 0:
            cnt += 1
    return cnt

def str_common_word_freq(str1, str2, freqs):
    words, cnt = str1.split(), 0
    for word in words:
        if str2.find(word) >= 0:
            if word in freqs:
                cnt += 1 * freqs[word]
            else:
                cnt += 1
    return cnt

def str_whole_word(str1, str2, i_):
    cnt = 0
    while i_ < len(str2):
        i_ = str2.find(str1, i_)
        if i_ == -1:
            return cnt
        else:
            cnt += 1
            i_ += len(str1)
    return cnt

def fmean_squared_error(ground_truth, predictions):
    fmean_squared_error_ = mean_squared_error(ground_truth, predictions)**0.5
    return fmean_squared_error_

RMSE = make_scorer(fmean_squared_error, greater_is_better=False)

unnecessari_fields = ['id', 'relevance', 'search_term', 'product_title', 'product_description', 'product_info', 'attr', 'brand','attsall','attrs','product_info_attrs',
    "alltogether", "product_alltogethers", 'bullets', 'sizes', 'thingsYes', 'thingsNo', 'prod_acts']

class cust_regression_vals(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, hd_searches):
        d_col_drops = unnecessari_fields
        hd_searches = hd_searches.drop(d_col_drops, axis=1).values
        return hd_searches

class cust_txt_col(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        return data_dict[self.key].apply(str)

def celll(n):
    v = float(n)
    if v > 3.0:
        return str(3)
    return str(v)

if __name__ == '__main__':
    df_train = pd.read_csv('../input/trainSpellCheck.csv', encoding="ISO-8859-1")#[:1000] #update here
    df_test = pd.read_csv('../input/testSpellCheck.csv', encoding="ISO-8859-1")#[:1000] #update here
    df_pro_desc = pd.read_csv('../input/product_descriptions.csv')#[:1000] #update here
    df_attrs = pd.read_csv('../input/pau-atts-separate.csv')#[:1000] #update here
    df_attr2 = pd.read_csv('../input/pau-atts2.csv')#[:1000] #update here


    df_attr = pd.read_csv('../input/attributes.csv')
    df_brand = df_attr[df_attr["name"] == "MFG Brand Name"][["product_uid", "value"]].rename(columns={"value": "brand"})
    num_train = df_train.shape[0]
    df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)
    df_all = pd.merge(df_all, df_pro_desc, how='left', on='product_uid')
    df_all = pd.merge(df_all, df_brand, how='left', on='product_uid')
    df_all = pd.merge(df_all, df_attrs, how='left', on='product_uid')
    df_all = pd.merge(df_all, df_attr2, how='left', on='product_uid')
    print("--- Files Loaded: %s minutes ---" % round(((time.time() - start_time)/60), 2))

    df_all = pd.read_csv('df_all.csv', encoding="ISO-8859-1", index_col=0)
    df_train = df_all.iloc[:num_train]
    df_test = df_all.iloc[num_train:]
    id_test = df_test['id']
    y_train = df_train['relevance'].values
    X_train = df_train[:]
    X_test = df_test[:]
    print("--- Features Set: %s minutes ---" % round(((time.time() - start_time)/60), 2))

    unnecessari_fields = ['id', 'relevance', 'search_term', 'product_title', 'product_description', 'product_info', 'attr', 'brand','attsall','attrs','product_info_attrs',
        "alltogether", "product_alltogethers", 'bullets', 'sizes', 'thingsYes', 'thingsNo', 'prod_acts', 'len_of_attsall', 'theRest'
        # ,'query_in_alltogether', 'ratio_word_in_alltogether', 'ratio_word_in_alltogether', 'word_in_alltogether_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_alltogether'
        # ,'query_in_attrs', 'ratio_word_in_attrs', 'word_in_attrs_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_attrs'
        ]



    tfidf = TfidfVectorizer(ngram_range=(1, 1), stop_words='english')
    tsvd = TruncatedSVD(n_components=10, random_state=2016)


    clf = Pipeline([
        ('union', FeatureUnion(transformer_list=[
            ('cst', cust_regression_vals()),
            ('txt1', Pipeline([
                ('s1', cust_txt_col(key='search_term')),
                ('tfidf1', tfidf),
                ('tsvd1', tsvd)


                # ,('tsvd1', tsvd)
            ]))]))])



    X_train = clf.fit_transform(X_train)

    import pickle

    pickle.dump(X_train, open( 'X_train.pickle', "wb" ))

    X_test = clf.fit_transform(X_test)

    pickle.dump(X_test, open( 'X_test.pickle', "wb" ))
