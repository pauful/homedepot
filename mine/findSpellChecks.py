import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer
from sklearn.externals import joblib
import string
import re
from nltk.corpus import stopwords
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import wordnet as wn
from sets import Set
import enchant
import sys

nltk.download('stopwords')
d = enchant.Dict("en_US")
cachedStopWords = stopwords.words("english")

stemmer = SnowballStemmer('english')

df_train = pd.read_csv('../input/train.csv', encoding="ISO-8859-1")
df_test = pd.read_csv('../input/test.csv', encoding="ISO-8859-1")

num_train = df_train.shape[0]

def spell_check_stem(str):

    str = str.lower()
    str = re.sub('[%s]' % string.punctuation,' ',str)

    for w in str.split():
        if d.check(w) != True:
            print w
            listM = d.suggest(w)
            if len(listM) > 0:
                print listM

            else:
                print '###None###'

    return str

def spell_check_stem2(str):

    prog = re.compile('([0-9]+)\.([a-zA-Z]+)')


    for w in str.split():
        if prog.match(w):
            print w

    return str

print 'ay'
df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)

df_all['search_term'].map(lambda x:spell_check_stem(x))
