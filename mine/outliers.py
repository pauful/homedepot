#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :

import time
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, ExtraTreesRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import PassiveAggressiveRegressor, SGDRegressor, LogisticRegression
from sklearn.linear_model import ElasticNet
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
from nltk.stem.porter import *
from sklearn.preprocessing import LabelBinarizer
from sklearn.isotonic import IsotonicRegression
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression, chi2
from sklearn.neighbors import KNeighborsRegressor
from sklearn.cluster import DBSCAN, KMeans

import xgboost as xgb

strNum = {'zero':0, 'one':1, 'two':2, 'three':3, 'four':4, 'five':5, 'six':6, 'seven':7, 'eight':8, 'nine':9}

class DenseTransformer(BaseEstimator, TransformerMixin):

    def transform(self, X, y=None, **fit_params):
        return X.todense()

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y, **fit_params)
        return self.transform(X)

    def fit(self, X, y=None, **fit_params):
        return self

import HTMLParser
htmlParser = HTMLParser.HTMLParser()
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
def str_stem(s):

    if isinstance(s, basestring) or isinstance(s,str):
        # print s
        try:
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
            s = htmlParser.unescape(s)
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
        except UnicodeDecodeError:
            print '>>> OH shit'
            print type(s)
            print s
            # raise
            s = unicodedata.normalize('NFD', unicode(s.decode('utf-8'))).encode('ascii', 'ignore')
            s = htmlParser.unescape(s)
            s = unicodedata.normalize('NFD', unicode(s)).encode('ascii', 'ignore')
    else :
        s = str(s)
    s = htmlParser.unescape(s)
    s = re.sub(r"(\w)\.([A-Z])", r"\1 \2", s) #Split words with a.A
    s = s.lower()
    s = s.replace("  ", " ")
    s = s.replace(",", "") #could be number / segment later
    s = s.replace("$", " ")
    s = s.replace("?", " ")
    s = s.replace("-", " ")
    s = s.replace("//", "/")
    s = s.replace("..", ".")
    s = s.replace(" / ", " ")
    s = s.replace(" \\ ", " ")
    s = s.replace(".", " . ")
    s = re.sub(r"(^\.|/)", r"", s)
    s = re.sub(r"(\.|/)$", r"", s)
    s = re.sub(r"([0-9])([a-z])", r"\1 \2", s)
    s = re.sub(r"([a-z])([0-9])", r"\1 \2", s)
    s = s.replace(" x ", " xbi ")
    s = re.sub(r"([a-z])( *)\.( *)([a-z])", r"\1 \4", s)
    s = re.sub(r"([a-z])( *)/( *)([a-z])", r"\1 \4", s)
    s = s.replace("*", " xbi ")
    s = s.replace(" by ", " xbi ")
    s = re.sub(r"([0-9])( *)\.( *)([0-9])", r"\1.\4", s)
    s = re.sub(r"([0-9]+)( *)(inches|inch|in|')\.?", r"\1in. ", s)
    s = re.sub(r"([0-9]+)( *)(foot|feet|ft|'')\.?", r"\1ft. ", s)
    s = re.sub(r"([0-9]+)( *)(pounds|pound|lbs|lb)\.?", r"\1lb. ", s)
    s = re.sub(r"([0-9]+)( *)(square|sq) ?\.?(feet|foot|ft)\.?", r"\1sq.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(cubic|cu) ?\.?(feet|foot|ft)\.?", r"\1cu.ft. ", s)
    s = re.sub(r"([0-9]+)( *)(gallons|gallon|gal)\.?", r"\1gal. ", s)
    s = re.sub(r"([0-9]+)( *)(ounces|ounce|oz)\.?", r"\1oz. ", s)
    s = re.sub(r"([0-9]+)( *)(centimeters|cm)\.?", r"\1cm. ", s)
    s = re.sub(r"([0-9]+)( *)(milimeters|mm)\.?", r"\1mm. ", s)
    # s = s.replace(unicode("°"), " degrees ")
    s = re.sub(r"([0-9]+)( *)(degrees|degree)\.?", r"\1deg. ", s)
    s = s.replace(" v ", " volts ")
    s = re.sub(r"([0-9]+)( *)(volts|volt)\.?", r"\1volt. ", s)
    s = re.sub(r"([0-9]+)( *)(watts|watt)\.?", r"\1watt. ", s)
    s = re.sub(r"([0-9]+)( *)(amperes|ampere|amps|amp)\.?", r"\1amp. ", s)
    s = s.replace("  ", " ")
    s = s.replace(" . ", " ")
    s = (" ").join([str(strNum[z]) if z in strNum else z for z in s.split(" ")])
    s = (" ").join([stemmer.stem(z) for z in s.split(" ")])
    s = s.lower()
    s = s.replace("toliet", "toilet")
    s = s.replace("airconditioner", "air conditioner")
    s = s.replace("vinal", "vinyl")
    s = s.replace("vynal", "vinyl")
    s = s.replace("skill", "skil")
    s = s.replace("snowbl", "snow bl")
    s = s.replace("plexigla", "plexi gla")
    s = s.replace("rustoleum", "rust oleum")
    s = s.replace("whirpool", "whirlpool")
    s = s.replace("whirlpoolga", "whirlpool ga")
    s = s.replace("whirlpoolstainless", "whirlpool stainless")
    return s
    #else:
    #    return "null"

def hasNumbers(inputString):
    return 1 + len(re.findall(r'\d+', inputString))

def numbersFound(search, text):
    cnt = 1
    for numbers in re.findall(r'\d+', search):
        if text.find(numbers) > -1:
            cnt += 1
    return cnt

def seg_words(str1, str2):
    str2 = str2.lower()
    str2 = re.sub("[^a-z0-9./]", " ", str2)
    str2 = [z for z in set(str2.split()) if len(z) > 2]
    words = str1.lower().split(" ")
    s9 = []
    for word in words:
        if len(word) > 3:
            s1 = []
            s1 += segmentit(word, str2, True)
            if len(s9) > 1:
                s9 += [z for z in s1 if z not in ['er', 'ing', 's', 'less'] and len(z) > 1]
            else:
                s9.append(word)
        else:
            s9.append(word)
    return (" ".join(s9))

def segmentit(s, txt_arr, t):
    st = s
    r = []
    for j in xrange(len(s)):
        for word in txt_arr:
            if word == s[:-j]:
                r.append(s[:-j])
                #print(s[:-j],s[len(s)-j:])
                s = s[len(s)-j:]
                r += segmentit(s, txt_arr, False)
    if t:
        i = len(("").join(r))
        if not i == len(st):
            r.append(st[i:])
    return r

def str_common_word(str1, str2):
    words, cnt = str1.split(), 0
    for word in words:
        if str2.find(word) >= 0:
            cnt += 1
    return cnt

def str_two_word(str1, str2):
    words, cnt = str1.split(), 0
    for idx, word in enumerate(words):
        if(idx < len(words)-1):
            twoWords = words[idx] + ' ' + words[idx + 1]
            if str2.find(twoWords) >= 0:
                cnt += 1
    return cnt

def str_two_word_freq(str1, str2, freqs):
    words, cnt = str1.split(), 0
    for idx, word in enumerate(words):
        if(idx < len(words)-1):
            twoWords = words[idx] + ' ' + words[idx + 1]
            if str2.find(twoWords) >= 0:
                if words[idx] in freqs:
                    cnt += 1 * freqs[words[idx]]
                else:
                    cnt += 1
                if words[idx+1] in freqs:
                    cnt += 1 * freqs[words[idx+1]]
                else:
                    cnt += 1
    return cnt

def str_three_word(str1, str2):
    words, cnt = str1.split(), 0
    for idx, word in enumerate(words):
        if(idx < len(words)-2):
            twoWords = words[idx] + ' ' + words[idx + 1] + ' ' + words[idx + 2]
            if str2.find(twoWords) >= 0:
                cnt += 1
    return cnt

from matplotlib import colors

def str_common_word_colors(str1, str2):
    words, cnt = str1.split(), 0
    notIn = 0
    for word in words:
        if str2.find(word) >= 0:
            cnt += 1
        if word in list(colors.cnames):
            notIn += 1

    if cnt == 0:
        return notIn
    return cnt

def str_common_word_freq(str1, str2, freqs):
    words, cnt = str1.split(), 0
    for word in words:
        if str2.find(word) >= 0:
            if word in freqs:
                cnt += 1 * freqs[word]
            else:
                cnt += 1
    return cnt

def cal_word_freq(str1, freqs):
    words, cnt = str1.split(), 0
    for word in words:
        if word in freqs:
            cnt += freqs[word]
    return cnt

def str_whole_word(str1, str2, i_):
    cnt = 0
    while i_ < len(str2):
        i_ = str2.find(str1, i_)
        if i_ == -1:
            return cnt
        else:
            cnt += 1
            i_ += len(str1)
    return cnt

def fmean_squared_error(ground_truth, predictions):
    fmean_squared_error_ = mean_squared_error(ground_truth, predictions)**0.5
    return fmean_squared_error_

RMSE = make_scorer(fmean_squared_error, greater_is_better=False)


    # df_all['query_three_in_title'] = df_all['product_info'].map(lambda x: str_three_word(x.split('|')[0], x.split('|')[1]))
    # df_all['query_three_in_description'] = df_all['product_info'].map(lambda x: str_three_word(x.split('|')[0], x.split('|')[2]))
    # df_all['query_three_in_alltogether'] = df_all['product_alltogethers'].map(lambda x: str_three_word(x.split('|')[0], x.split('|')[1]))
    #
    # df_all['ratio_query_three_in_title'] = df_all['query_three_in_title']/df_all['len_of_query']
    # df_all['ratio_query_three_in_description'] = df_all['query_three_in_description']/df_all['len_of_query']
    # df_all['ratio_query_three_in_alltogether'] = df_all['query_three_in_alltogether']/df_all['len_of_query']

unnecessari_fields_final = ['search_term','id', 'relevance', 'product_title','product_description', 'another_brand'
    ,'query_words_not_in_color', 'search_term_has_Number'
    ,'query_words_color', 'len_of_brand', 'query_words_thingsYes'
    ,'query_two_in_atts', 'ratio_query_two_in_atts' #, 'ratio_len'
    ,'query_three_in_title', 'query_three_in_description', 'query_three_in_alltogether'
    , 'ratio_query_three_in_title', 'ratio_query_three_in_description', 'ratio_query_three_in_alltogether'
    # ,'similarity', 'similarityAll'
    # , 'query_last_word_in_title_last_word'
    # , 'brand_feature', 'product_uid',
    # ,'query_in_alltogether', 'ratio_word_in_alltogether', 'ratio_word_in_alltogether', 'word_in_alltogether_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_alltogether'
    # ,'query_in_attrs', 'ratio_word_in_attrs', 'word_in_attrs_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_attrs'
    ]

only_those_fields = ['ratio_word_in_attrs_freq', 'query_last_word_in_title', 'query_last_word_in_description', 'word_in_title', 'word_in_title_freq'
, 'word_in_description_freq', 'ratio_word_in_title_freq', 'ratio_word_in_description_freq', 'ratio_title', 'ratio_description', 'word_in_alltogether_freq', 'query_last_word_in_alltogether'
, 'query_last_word_in_bullets', 'similarity', 'similarityAll' ,'product_uid']

class ModelTransformer(TransformerMixin):

    def __init__(self, model):
        self.model = model

    def fit(self, *args, **kwargs):
        self.model.fit(*args, **kwargs)
        return self

    def transform(self, X, **transform_params):
        return pd.DataFrame(self.model.predict(X))

class cust_regression_vals(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, hd_searches):
        d_col_drops = unnecessari_fields_final
        hd_searches = hd_searches.drop(d_col_drops, axis=1).values
        # hd_searches = hd_searches[only_those_fields]
        return hd_searches
import pickle
class pca_things(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, hd_searches):

        hd_searches = pickle.load(open('pcaSearch.p', 'rb'))
        return hd_searches[:num_train]

def removenums(s):
    newWord = ''
    for w in s.split(' '):
        aa = re.match('(.*[0-9].*)', w)
        if aa:
            continue
        else:
            newWord = newWord + ' ' + w
    return newWord

brandId = {}
def generateIdBrand(brand, productId):
    brand = str_stem(brand)
    if brand not in brandId and brand != '' and brand != ' ':
        brandId[brand] = productId

from sets import Set
another_brans= Set([])

def another_brand(search, brand, prod):
    another = 0
    for b in brandId.keys():
        if search.find(b) >= 0 and b != ' ':
            if brandId[b] != prod:

                # print prod +' != ' + brandId[b]
                # print b + ' == ' + search

                another += 1
                another_brans.add(b)
            else:
                return 0
    return another


class cust_txt_col(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        # data_dict[self.key].map(lambda x: removenums(x))
        return data_dict[self.key].apply(str)

def celll(n):
    v = float(n)
    if v > 3.0:
        return str(3)
    return str(v)


import pandas as pd
from gensim import corpora, models

def get_topic_features(col, search_col):
    """Derive topic features from a text pandas series"""
    # generate topics for corpora
    print 'creating dict'
    colname = col.name
    col = col.astype(str).apply(lambda x:x.split())
    dictionary = corpora.Dictionary(col)
    # pickle.dump(dictionary, open( 'dictionary.pickle', "wb" ))
    # dictionary = pickle.load(open( 'dictionary.pickle', "rb" ))
    corpus = [dictionary.doc2bow(text) for text in col]
    print 'now tfidf'
    tfidf = models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    print 'topiqueando'
    lda = models.LdaModel(corpus_tfidf, id2word=dictionary, num_topics=10, passes=2, iterations=50)
    # pickle.dump(lda, open( 'ldamodel.pickle', "wb" ))
    # lda = pickle.load(open( 'ldamodel.pickle', "rb" ))
    # print lda.print_topics(-1)
    # get topic distribution for doc
    print 'PRINTEDDDD'
    def get_topics(words): return dict(lda[dictionary.doc2bow(words)])
    search_col = search_col.astype(str).apply(lambda x:x.split())
    topics_df = pd.DataFrame(search_col.apply(get_topics).tolist()).fillna(0.001)
    topics_df.columns = ['topic_'+str(cn)+'_'+colname for cn in topics_df.columns]
    print topics_df
    return topics_df

from gensim.similarities.docsim import Similarity


def getSim(prod, desc, index, dictionary, prods):
    # print 'get sim'

    docbb = dictionary.doc2bow(desc.split())
    # print desc.split()
    # print docbb

    simis = index[docbb]
    # print simis
    ind = prods.index(int(prod))
    # print ind
    return simis[ind]

def getSimilarityObj(df_all):
    simObj = {}
    print df_all.shape
    df_all = df_all.drop_duplicates(['product_uid'])
    print df_all.shape
    prods =  df_all["product_uid"].values.tolist()
    print 'stem'
    # descript['product_description'] = descript['product_description'].map(lambda x: str_stem(x))
    # descript.to_csv('descriptions_stemmed.csv')
    print 'sym'
    col = df_all["alltogether"]
    print 'creating dict'
    colname = col.name
    col = col.astype(str).apply(lambda x:x.split())
    dictionary = corpora.Dictionary(col)
    print len(dictionary.values())
    # pickle.dump(dictionary, open( 'dictionary.pickle', "wb" ))
    # dictionary = pickle.load(open( 'dictionary.pickle', "rb" ))
    corpus = [dictionary.doc2bow(text) for text in col]
    print 'now tfidf'
    tfidf = models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]

    print df_all["product_description"].values[0]

    index = Similarity('indexxx.ii',corpus_tfidf, num_features= 200000)
    print 'save'
    return index, dictionary, prods
    # index.save('similarity.obj')
    # pickle.dump(simObj, open( 'sims.pickle', "wb" ))
    # pickle.dump(dictionary, open( 'dixtionary.pickle', "wb" ))

from sklearn import svm

if __name__ == '__main__':
    df_train = pd.read_csv('../input/trainSpellCheck2.csv', encoding="ISO-8859-1")#[:1000] #update here
    df_test = pd.read_csv('../input/testSpellCheck2.csv', encoding="ISO-8859-1")#[:1000] #update here
    df_pro_desc = pd.read_csv('descriptions_stemmed.csv')#[:1000] #update here
    df_attrs = pd.read_csv('../input/pau-attributes-all.csv')#[:1000] #update here
    df_attr2 = pd.read_csv('../input/pau-atts2.csv')#[:1000] #update here

    # df_all = pd.read_csv('df_all2.csv', encoding="ISO-8859-1", index_col=0)
    df_all = pd.read_csv('df_all_jut_needed_fields3.csv', encoding="ISO-8859-1", index_col=0)


    unnecessari_fields_final = ['search_term','id', 'relevance', 'product_title','product_description', 'another_brand'
        ,'query_words_not_in_color', 'search_term_has_Number'
        ,'query_words_color', 'len_of_brand', 'query_words_thingsYes'
        ,'query_two_in_atts', 'ratio_query_two_in_atts' #, 'ratio_len'
        ,'query_three_in_title', 'query_three_in_description', 'query_three_in_alltogether'
        , 'ratio_query_three_in_title', 'ratio_query_three_in_description', 'ratio_query_three_in_alltogether'
        ,'query_first_word_in_attrs', 'query_first_word_in_title_last_word'
        # , 'ratio_word_bullets', 'ratio_word_bullets_freq'
        # ,'similarity', 'similarityAll'
        # , 'query_last_word_in_title_last_word'
        # , 'brand_feature', 'product_uid',
        # ,'query_in_alltogether', 'ratio_word_in_alltogether', 'ratio_word_in_alltogether', 'word_in_alltogether_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_alltogether'
        # ,'query_in_attrs', 'ratio_word_in_attrs', 'word_in_attrs_freq', 'ratio_word_in_attrs_freq', 'query_last_word_in_attrs'
        ]
    df_all = df_all.drop(unnecessari_fields_final, axis=1)
    # df_all['product_uid_str'] = df_all['product_uid_str'].map(lambda x: str(x))
    # df_all['product_uid_str'] = df_all['product_uid_str'].astype(str)
    #



    # index, dictionary, prods = getSimilarityObj(df_all)
    # quit()
    #
    #
    # df_attr = pd.read_csv('../input/attributes.csv')
    # df_brand = df_attr[df_attr["name"] == "MFG Brand Name"][["product_uid", "value"]].rename(columns={"value": "brand"})
    # df_brand['product_uid_str'] = df_brand['product_uid'].map(lambda x: str(int(x)))
    # df_brand['allin'] = df_brand['brand'] + '|' + df_brand['product_uid_str']
    #
    # df_brand['allin'] = df_brand['allin'].fillna('|')
    # df_brand['allin'].map(lambda x: generateIdBrand(x.split('|')[0],x.split('|')[1]))

    num_train = df_train.shape[0]



    # print df_all.columns.values
    df_train = df_all.iloc[:num_train]
    # df_test = df_all.iloc[num_train:]
    # id_test = df_test['id']
    # y_train = df_train['relevance'].values
    # X_train = df_train[:]
    # X_test = df_test[:]
    print("--- Features Set: %s minutes ---" % round(((time.time() - start_time)/60), 2))

    clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
    clf.fit(df_train)
    print clf.predict(df_train)
