#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :

import time
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, ExtraTreesRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import PassiveAggressiveRegressor, SGDRegressor, LogisticRegression
from sklearn.linear_model import ElasticNet
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
from nltk.stem.porter import *
from sklearn.preprocessing import LabelBinarizer
from sklearn.isotonic import IsotonicRegression
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression, chi2
from sklearn.neighbors import KNeighborsRegressor

import xgboost as xgb

def printBigs(idi, one, two):
    print 'iiiiiiiiiiiin'
    # print one
    # print idi + ' - ' + one  + ' - ' + two
    df_alli = pd.read_csv('df_all_jut_needed_fields.csv', encoding="ISO-8859-1")#[:1000] #update here
    cnt = 0
    idlist=[]
    for idx, val in enumerate(idi):
        if abs(one[idx] - two[idx]) > 1:# and one[idx] > 2:
            print str(val) + ' - ' + str(two[idx])
            idlist.append(val)
            cnt += 1
            # print str(cnt)
    df_alli[df_alli['id'].isin(idlist)].to_csv('veryDifferent.csv')
    print 'Total:'
    print cnt

if __name__ == '__main__':
    df_train = pd.read_csv('submissionTrainValues.csv', encoding="ISO-8859-1")#[:1000] #update here
    df_test = pd.read_csv('../input/trainSpellCheck.csv', encoding="ISO-8859-1")#[:1000] #update here

    num_train = int(round(df_train.shape[0] * 0.75))
    num_total = df_train.shape[0]

    df_all = pd.merge(df_train, df_test, how='left', on='id')
    print("--- Files Loaded: %s minutes ---" % round(((time.time() - start_time)/60), 2))
    printBigs(df_all['id'].tolist(),df_all['relevance'].tolist(),df_all['relevance2'].tolist())
    # df_all['relevance_add'] = str(df_all['id']) + '|' + str(df_all['relevance']) + '|' + str(df_all['relevance2'])
    # df_all['relevance_add'].map(lambda x: printBigs(x.split('|')[0],x.split('|')[1],x.split('|')[2]))



    # df_all = df_all.drop(unnecessari_fields, axis=1)
    # df_all.to_csv('df_all_jut_needed_fields.csv')
    # print 'FINNNNNN'
    # quit()
