#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :

import time
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import mean_squared_error, make_scorer
from nltk.stem.porter import *
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression

import xgboost as xgb

df_s1 = pd.read_csv('best/submission45818.csv', encoding="ISO-8859-1")#[:1000] #update here
df_s2 = pd.read_csv('best/submission45866.csv', encoding="ISO-8859-1")#[:1000] #update here
df_s3 = pd.read_csv('best/submission45915.csv', encoding="ISO-8859-1")#[:1000] #update here
df_s4 = pd.read_csv('best/submission45930.csv', encoding="ISO-8859-1")#[:1000] #update here
df_s5 = pd.read_csv('best/submission45947.csv', encoding="ISO-8859-1")#[:1000] #update here

# df_s1['relevance'] = (df_s1['relevance'] + df_s2['relevance'] + df_s3['relevance'] + df_s4['relevance']) / 4

df_s1['relevance1'] = df_s2['relevance']
df_s1['relevance2'] = df_s3['relevance']
df_s1['relevance3'] = df_s4['relevance']
df_s1['relevance4'] = df_s5['relevance']


def directing(original, diff1, diff2, diff3, diff4):

    diff1 = float(diff1)
    diff2 = float(diff2)
    diff3 = float(diff3)
    diff4 = float(diff4)
    original = float(original)

    ret = original
    if original < diff1 and diff1 < diff2 and diff2 < diff3 and diff3 < diff4:
        print 'change ++ ' + str((diff4 - diff1))
        ret += (diff4 - diff3)
        return ret
    if original > diff1 and diff1 > diff2 and diff2 > diff3 and diff3 > diff4:
        print 'change -- ' + str((diff1 - diff4))
        ret -= (diff3 - diff4)
        return ret

    return ret

print df_s1

df_s1['relevance'] = df_s1['relevance'].apply(str)
df_s1['relevance1'] = df_s1['relevance1'].apply(str)
df_s1['relevance2'] = df_s1['relevance2'].apply(str)
df_s1['relevance3'] = df_s1['relevance3'].apply(str)
df_s1['relevance4'] = df_s1['relevance4'].apply(str)

df_s1['relevanceEnd'] = df_s1['relevance'] + '|' + df_s1['relevance1'] + '|' + df_s1['relevance2'] + '|' + df_s1['relevance3'] + '|' + df_s1['relevance4']
df_s1['relevanceSubmit'] = df_s1['relevanceEnd'].map(lambda x: directing(x.split('|')[0],x.split('|')[1],x.split('|')[2],x.split('|')[3],x.split('|')[4]))

# print df_s1['relevance'].sum(axis=0)
# print df_s2['relevance'].sum(axis=0)
# print df_s3['relevance'].sum(axis=0)
# print df_s4['relevance'].sum(axis=0)

def celll(n):
    v = float(n)
    if v > 3.0:
        return str(3)
    return str(v)

# df_all = pd.merge(df_all, df_brand, how='left', on='product_uid')

# pp = pd.DataFrame({"id": id_test, "relevance": y_pred})

# print pd['relevance']
# pp['relevance'] = pp['relevance'].map(lambda x: celll(x))

df_s1['relevanceSubmit'] = df_s1['relevanceSubmit'].map(lambda x: celll(x))

df_s1.to_csv('submissionSum.csv', columns=['id','relevanceSubmit'], index=False)
print("--- Training & Testing: %s minutes ---" % round(((time.time() - start_time)/60), 2))
