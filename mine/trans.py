#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import json
import re

csvfile = open('../input/attributes.csv', 'r')

listP = {}

fieldnames = ("product_uid","name","value")
reader = csv.DictReader( csvfile, fieldnames)
i = 0
for row in reader:

    try:
        if row['product_uid'] not in listP:

            listP[row['product_uid']] = {
                'product_uid': int(row['product_uid']),
                'bullets': '',
                'sizes': '',
                'thingsYes': '',
                'thingsNo': '',
                'theRest': '',
                'color': ''
            }
        if row['name'].startswith('Bullet'):
            if row['name'] != 'Bullet22' or row['name'] != 'Bullet20' or row['value'] != 'Note: product may vary by store':
                listP[row['product_uid']]['bullets'] = listP[row['product_uid']]['bullets'] + ' ' + row['value']
                continue

        if row['value'] == 'Yes':
            listP[row['product_uid']]['thingsYes'] = listP[row['product_uid']]['thingsYes'] + " " + row['name']
            continue

        if row['value'] == 'No':
            listP[row['product_uid']]['thingsNo'] = listP[row['product_uid']]['thingsNo'] + " " + row['name']
            continue

        la = re.match(r'.* \((.*)\)', row['name'] )
        if la:
            listP[row['product_uid']]['sizes'] = listP[row['product_uid']]['sizes'] + " " + row['value'] + la.group(1) + ' ' + row['value']
            continue

        if row['name'].startswith('Color'):
            listP[row['product_uid']]['color'] = listP[row['product_uid']]['color'] + " " + row['value']
            continue

        listP[row['product_uid']]['theRest'] = listP[row['product_uid']]['theRest'] + " " + row['value']

    except ValueError as e:

        print 'hh'+row['product_uid']+'hh'




print list(listP.values())[0]

with open('../input/pau-atts2.csv', 'wb') as output_file:
    dict_writer = csv.DictWriter(output_file, ['product_uid', 'bullets', 'sizes', 'thingsYes', 'thingsNo', 'theRest', 'color'])
    dict_writer.writeheader()
    dict_writer.writerows(list(listP.values()))
